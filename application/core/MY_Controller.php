<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    protected $userData = "";

    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->userData = $this->session->userdata('userdata');
    }

    public function my_theme($url, $data = [])
    {
        $user_id = $this->userData['user_id'];
        if (empty($user_id)) {
            $user_id = 0;
        }

        $data["title"]     = array_key_exists('title', $data) && !empty($data['title']) ? $data['title'] : "";
        $data["menu_id"]     = array_key_exists('menu_id', $data) && !empty($data['menu_id']) ? $data['menu_id'] : "0";

        $data['sidebar'] = $this->db->query(
            "SELECT
                DISTINCT mm.menu_id,
                mm.menu_name ,
                mm.menu_level ,
                mm.menu_url ,
                mm.menu_icon ,
                mm.jenis_menu
            from
                ms_menu mm
            inner join group_menu gm on
                gm.menu_id = mm.menu_id
            inner join group_user gu on
                gu.group_id = gm.group_id
            WHERE
                mm.menu_status = 1
                and (mm.jenis_menu = 1 or mm.jenis_menu = 2)
                and gu.user_id = $user_id
            order by
                mm.menu_code"
        )->result();

        $data['topbar'] = [];

        $data['footer'] = array(
            "copyright" => "Copyright &copy; Tadin " . date("Y"),
            "logout"    => site_url("auth/logout"),
        );

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar', $data);
        $this->load->view('template/topbar', $data);
        $this->load->view($url, $data);
        $this->load->view('template/footer', $data);
    }
}
