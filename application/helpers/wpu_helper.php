<?php

function is_logged_in()
{
    $ci =  get_instance();
    if (!$ci->session->userdata('userdata')['is_login']) {
        redirect('auth/show');
    } else {
        $user_id = $ci->session->userdata('userdata')['user_id'];
        $menu = '';

        if ($ci->uri->segment(1) == 'admin') {
            $menu = $ci->uri->segment(1) . '/' . $ci->uri->segment(2);
        } else {
            $menu = $ci->uri->segment(1);
        }

        $sql = "SELECT
                    DISTINCT mm.menu_id
                FROM
                    ms_menu mm
                inner join group_menu gm on
                    gm.menu_id = mm.menu_id
                INNER JOIN group_user gu on
                    gu.group_id = gm.group_id
                where 
                    mm.menu_url like '$menu%'
                    and gu.user_id = $user_id";

        $userAccess = $ci->db->query($sql);
        if ($userAccess->num_rows() < 1) {
            return redirect('dashboard');
        }
    }
}
