<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendaftaran extends MY_Controller
{
    public $path = './assets/global/tugas_akhir/daftar';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mahasiswa/pendaftaran_m');
    }


    public function index()
    {
        $data['dosen'] = $this->pendaftaran_m->get_dosen();
        $data['mhs'] = $this->pendaftaran_m->get_userdata($this->userData['profil_id']);
        if (!empty($data['mhs']->dt_id)) {
            $dospem = $this->pendaftaran_m->get_dospem($data['mhs']->dt_id);
            foreach ($dospem as $k => $v) {
                if ($k == 0) {
                    $data['mhs']->dospem1 = $v->dosen_id;
                    $data['mhs']->dtd_id1 = $v->dtd_id;
                } else {
                    $data['mhs']->dospem2 = $v->dosen_id;
                    $data['mhs']->dtd_id2 = $v->dtd_id;
                }
            }
        } else {
            $data['mhs']->dospem1 = 0;
            $data['mhs']->dospem2 = 0;
            $data['mhs']->dtd_id1 = '';
            $data['mhs']->dtd_id2 = '';
        }
        $this->load->view('mahasiswa/pendaftaran_v', $data);
    }


    // save
    public function save()
    {
        $res = [];
        $dt_id = $this->input->post('dt_id', true);

        $data = [
            "mhs_id" => $this->input->post("d_mhs_id"),
            "jurusan_id" => $this->input->post("d_jurusan_id"),
            "dt_judul" => $this->input->post("dt_judul"),
        ];

        if (array_key_exists('dt_sinopsis', $_FILES)) {
            $data["dt_sinopsis_name"] = $_FILES["dt_sinopsis"]["name"];
        }

        if (empty($dt_id)) {
            $dt_id = $this->pendaftaran_m->add($data)['id'];
        } else {
            $this->pendaftaran_m->update($data, $dt_id);
        }

        $insert = 0;

        foreach ($this->input->post('detail') as $key => $value) {
            $data_detail[$key] = [
                "dosen_id" => $value['dosen_id'],
            ];

            if ($value['dtd_id'] != '') {
                $data_detail[$key]['dtd_id'] = $value['dtd_id'];
            } else {
                $data_detail[$key]['dt_id'] = $dt_id;
                $data_detail[$key]['dtd_dospem_ke'] =  $key + 1;
                $data_detail[$key]['dtd_status'] = 0;
                $insert++;
            }
        }

        if ($insert > 0) {
            $res = $this->pendaftaran_m->add($data_detail, 'detail');
        } else {
            $res = $this->pendaftaran_m->update($data_detail, 'dtd_id', 'detail');
        }

        // if ($this->input->post('is_delete') == 'yes' && $res['status'] == 'true') {
        //     $image = $this->db->get_where('mahasiswa', ['mhs_id' => $res['id']])->row()->photo;
        //     if (!empty($image) && file_exists(realpath($this->path . '/' . $image))) {
        //         unlink(realpath($this->path . '/' . $image));
        //     }
        //     $data = [
        //         'photo' => null,
        //     ];
        //     $res = $this->pendaftaran_m->update($data, $res['id']);
        // }

        if (count($_FILES) > 0 && $dt_id > 0) {
            $config['upload_path']      = $this->path;
            $config['allowed_types']    = 'pdf|doc|docx';
            $config['max_size']         = '5120';
            $config['file_name']        = $dt_id;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('dt_sinopsis')) {
                $data = [
                    'dt_sinopsis_path' => $this->upload->data('file_name')
                ];
                if ($this->pendaftaran_m->update($data, $dt_id)['status'] == 'true') {
                    $res = [
                        'status' => 'true',
                        'id' => $dt_id,
                    ];
                } else {
                    $res = [
                        'status' => 'false',
                        'msg' => 'Gagal update foto',
                    ];
                }
            } else {
                $res = [
                    'status' => 'false',
                    'msg' =>  $this->upload->display_errors(),
                ];
            }
        }

        echo json_encode($res);
    }


    // delete
    public function hapus($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->pendaftaran_m->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }
}
