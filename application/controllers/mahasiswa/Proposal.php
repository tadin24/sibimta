<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proposal extends MY_Controller
{
    public $path = './assets/global/tugas_akhir/daftar';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mahasiswa/proposal_m');
        $this->load->model('global_m');
    }


    public function index()
    {
        $data['title'] = "Pengajuan Proposal Tugas Akhir";
        $data['menu_id'] = 20;
        $data['submenu'] = $this->global_m->get_submenu($data['menu_id']);
        $this->my_theme('mahasiswa/proposal_v', $data);
    }
}
