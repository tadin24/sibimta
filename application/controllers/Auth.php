<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_m');
    }


    public function show($jenis = '')
    {
        $data['title'] = "Login";
        $this->load->view('auth_v', $data);
    }


    // 
    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $userdata = $this->db->get_where('ms_user', ['username' => $username]);

        if ($userdata->num_rows() > 0) {
            $userdata = $userdata->row_array();
            if ($userdata['user_status'] == 1) {
                if (password_verify($password, $userdata['password'])) {
                    $res['status'] = true;
                    $res['url'] = base_url() . 'dashboard';
                    $userdata['is_login'] = 1;
                    $table = $userdata['jenis_profil'] == '2' ? 'pegawai' : 'mahasiswa';
                    $profil_id = $userdata['jenis_profil'] == '2' ? 'pegawai_id' : 'mhs_id';
                    $key_nama = $userdata['jenis_profil'] == '2' ? 'pegawai_nama' : 'mhs_name';
                    $profil = $this->db->get_where($table, [$profil_id => $userdata['profil_id']])->row_array();
                    $userdata['user_fullname'] = $profil[$key_nama];
                    $userdata['photo'] = $profil['photo'];
                    $this->session->set_userdata(['userdata' => $userdata]);
                } else {
                    $res['status'] = false;
                    $res['msg'] = "Password salah";
                }
            } else {
                $res['status'] = false;
                $res['msg'] = "User tidak aktif";
            }
        } else {
            $res['status'] = false;
            $res['msg'] = "Username belum terdaftar";
        }

        echo json_encode($res);
    }


    // 
    public function logout()
    {
        $this->session->sess_destroy();
        return redirect('auth/show');
    }
}
