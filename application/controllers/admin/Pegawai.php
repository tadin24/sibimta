<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends MY_Controller
{
    public $path = './assets/global/images/pegawai';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/pegawai_m');
    }


    public function index()
    {
        $data['title'] = "Pegawai";
        $data['kategori'] = $this->db->where('kp_status', 1)->order_by('kp_code', 'asc')->get('kategori_pegawai')->result();
        $data['menu_id'] = "4";
        $this->my_theme('admin/pegawai_v', $data);
    }


    // get data krisis
    public function get_data()
    {
        $columns = [
            "p.pegawai_id",
            "p.pegawai_nip",
            "p.pegawai_nama",
            "p.pegawai_status",

        ];
        $columns_search = [
            "p.pegawai_nip",
            "p.pegawai_nama",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->pegawai_m->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->pegawai_m->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->pegawai_id;

            if ($row->pegawai_status == 1) {
                $pegawai_status = "<span class='badge badge-success'>Aktif</span>";
            } else {
                $pegawai_status = "<span class='badge badge-danger'>Non Aktif</span>";
            }

            $action = '<a href="javascript:void(0);" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit">
                            <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a href="javascript:;" onclick="set_del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus">
                            <i class="fa fa-trash"></i>
                        </a>';
            $records["data"][] = array(
                $no++,
                $row->pegawai_nip,
                $row->pegawai_nama,
                $pegawai_status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "pegawai_nama" => $this->input->post("pegawai_nama"),
            "pegawai_gd" => $this->input->post("pegawai_gd"),
            "pegawai_gb" => $this->input->post("pegawai_gb"),
            "pegawai_nip" => $this->input->post("pegawai_nip"),
            "pegawai_nidn" => $this->input->post("pegawai_nidn"),
            "kp_id" => $this->input->post("kp_id"),
            "email" => $this->input->post("email"),
            "phone" => $this->input->post("phone"),
            "pegawai_status" => $this->input->post("pegawai_status"),
        ];

        if ($act == "add") {
            $res = $this->pegawai_m->add($data);
        } else {
            $id = $this->input->post('pegawai_id');
            $res = $this->pegawai_m->update($data, $id);
        }

        if ($this->input->post('is_delete') == 'yes' && $res['status'] == 'true') {
            $image = $this->db->get_where('pegawai', ['pegawai_id' => $res['id']])->row()->photo;
            if (!empty($image) && file_exists(realpath($this->path . '/' . $image))) {
                unlink(realpath($this->path . '/' . $image));
            }
            $data = [
                'photo' => null,
            ];
            $res = $this->pegawai_m->update($data, $res['id']);
        }

        if (count($_FILES) > 0 && $res['status'] == 'true') {
            $config['upload_path']      = $this->path;
            $config['allowed_types']    = 'gif|jpg|jpeg|png';
            $config['max_size']         = '2048';
            $config['file_name']        = $res['id'];
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('photo')) {
                $data = [
                    'photo' => $this->upload->data('file_name')
                ];
                if ($this->pegawai_m->update($data, $res['id'])['status'] == 'true') {
                    $res = [
                        'status' => 'true',
                        'id' => $res['id'],
                    ];
                } else {
                    $res = [
                        'status' => 'false',
                        'msg' => 'Gagal update foto',
                    ];
                }
            } else {
                $res = [
                    'status' => 'false',
                    'msg' =>  $this->upload->display_errors(),
                ];
            }
        }

        echo json_encode($res);
    }


    // 
    public function edit($id)
    {
        $data['data'] = $this->pegawai_m->edit($id);
        echo json_encode($data);
    }


    // delete
    public function hapus($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $image = $this->db->get_where('pegawai', ['pegawai_id' => $id])->row()->photo;
            if (!empty($image) && file_exists(realpath($this->path . '/' . $image))) {
                unlink(realpath($this->path . '/' . $image));
            }
            $res = $this->pegawai_m->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek pegawai_nip
    public function cek_data($jenis)
    {
        $res = "";
        $where = "";
        $act = $this->input->post("act");
        $input = $input_lama = "";

        if ($jenis == 'pegawai_nip') {
            $input = $this->input->post('pegawai_nip');
            $input_lama = $this->input->post('pegawai_nip_lama');
        }

        $where .= " AND $jenis = '$input'";

        if ($act == "edit") {
            $where .= " AND $jenis != '$input_lama'";
        }

        if ($this->pegawai_m->cek_data($where) > 0) {
            switch ($jenis) {
                case 'pegawai_nip':
                    $res = "NIP sudah digunakan!";
                    break;
            }
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }
}
