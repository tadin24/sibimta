<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_in_group extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/user_in_group_m');
    }


    public function index()
    {
        $data['title'] = "User In Group";
        $data['group'] = $this->user_in_group_m->get_group();
        $data['menu_id'] = "22";
        $this->my_theme('admin/user_in_group_v', $data);
    }


    // get data krisis
    public function get_data()
    {
        $columns = [
            "mu.user_id",
            "mu.user_id",
            "mu.username",
        ];
        $columns_search = [
            "mu.username",
        ];
        $draw = intval($this->input->post("draw"));
        $dari = $this->input->post('dari');
        $f_jenis_profil = $this->input->post('f_jenis_profil');
        $f_group_id = $this->input->post('f_group_id');
        $where     = "";
        $where1     = " mu.jenis_profil = $f_jenis_profil";
        $where2     = " gu.group_id = $f_group_id";

        $param = [
            'dari' => $dari,
            'where1' => $where1,
            'where2' => $where2,
        ];

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->user_in_group_m->get_total($where, $param);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->user_in_group_m->get_data($columns, $where, $order, $limit, $param);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $id = $row->user_id;

            $records["data"][] = array(
                $no++,
                '<input class="user-id" type="checkbox" value="' . $id . '" >',
                $row->username,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = "";

        $user_id = $this->input->post("user_id");
        $group_id = $this->input->post("group_id");
        $data = [];

        foreach ($user_id as $v) {
            $data[] = [
                'user_id' => $v,
                'group_id' => $group_id
            ];
        }

        $res = $this->user_in_group_m->add($data);

        echo json_encode($res);
    }


    // delete
    public function hapus()
    {
        $user_id = $this->input->post('user_id');
        $group_id = $this->input->post('group_id');
        $res = $this->user_in_group_m->delete($user_id, $group_id);
        echo json_encode($res);
    }
}
