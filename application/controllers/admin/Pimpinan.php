<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pimpinan extends MY_Controller
{
    public $group_id = 5;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/pimpinan_m');
    }


    public function index()
    {
        $data['title'] = "Pimpinan";
        $data['menu_id'] = "5";
        $this->my_theme('admin/pimpinan_v', $data);
    }


    // get data
    public function get_data()
    {
        $columns = [
            "gu.user_id",
            "p.pegawai_nip",
            "p.pegawai_nama",

        ];
        $columns_search = [
            "p.pegawai_nip",
            "p.pegawai_nama",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->pimpinan_m->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->pimpinan_m->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->user_id;

            $action = "<a class='btn btn-xs btn-danger' href='javascript:void(0);' title='hapus' onclick='set_del($id)'>
                        <i class='fa fa-trash'></i>
                    </a>";
            $records["data"][] = array(
                $no++,
                $row->pegawai_nip,
                $row->pegawai_nama,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = "";

        $data = [
            "user_id" => $this->input->post("user_id"),
            "group_id" => $this->group_id,
        ];

        $res = $this->pimpinan_m->add($data);

        echo json_encode($res);
    }


    // delete
    public function hapus($id)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->pimpinan_m->delete($id, $this->group_id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // ambil pegawai
    public function get_pegawai()
    {
        $res = $this->pimpinan_m->get_pegawai();

        echo json_encode($res);
    }
}
