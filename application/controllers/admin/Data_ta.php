<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data_ta extends MY_Controller
{
    public $path = './assets/data_ta/sinopsis';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/data_ta_m');
    }


    public function index()
    {
        $data['title'] = "Data Tugas Akhir";
        $data['menu_id'] = "25";
        $data['dosen'] = $this->data_ta_m->get_dosen();
        $data['jurusan'] = $this->db->where('jurusan_status', 1)->order_by('jurusan_id', 'asc')->get('jurusan')->result();
        $this->my_theme('admin/data_ta_v', $data);
    }


    // get data krisis
    public function get_data()
    {
        $columns = [
            "dt.dt_id",
            "m.mhs_name",
            "p.pegawai_nama",
            "dt.dt_status",
            "dtd.dtd_status",
            "dtd.dtd_dospem_ke",
            "dtd.dtd_id",
            "p.pegawai_gd",
            "p.pegawai_gb",
            "dt.verif_by",
        ];
        $columns_search = [
            "m.mhs_name",
            "p.pegawai_nama",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";
        $f_jurusan_id = $this->input->post("f_jurusan_id");
        $f_status = $this->input->post("f_status");

        $where     .= " AND dt.dt_status = $f_status";
        $where     .= " AND dt.jurusan_id = $f_jurusan_id";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->data_ta_m->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY dtd.dtd_dospem_ke," . $columns[$col] . " " . $dir;
        }

        $data = $this->data_ta_m->get_data($columns, $where, $order, $limit);
        // echo $this->db->last_query();
        $no   = 1 + $start;
        $dt_id = $i = 0;
        foreach ($data as $row) {
            if ($row->dtd_status == 0) {
                $dtd_status = '<span class="badge badge-warning">Proses</span>';
            } else if ($row->dtd_status == 1) {
                $dtd_status = '<span class="badge badge-danger">Ditolak</span>';
            } else {
                $dtd_status = '<span class="badge badge-success">Disetujui</span>';
            }

            if ($dt_id != $row->dt_id) {
                $records["data"][$i] = [
                    $no,
                    $row->mhs_name,
                    "$row->pegawai_gd $row->pegawai_nama, $row->pegawai_gb<br>$dtd_status"
                ];
                $dt_id = $row->dt_id;
            } else {
                $id = $row->dt_id;
                if ($row->dt_status == 0) {
                    $dt_status = '<span class="badge badge-warning">Proses</span>';
                    $verif = '';
                } else if ($row->dt_status == 1) {
                    $dt_status = '<span class="badge badge-danger">Ditolak</span>';
                    $verif =
                        '&nbsp;<a href="javascript:void(0);" onclick="verif(\'' . $id . '\',\'' . $row->dt_status . '\')" class="btn btn-success btn-sm" title="Verifikasi">
                            <i class="fa fa-check"></i>
                        </a>';
                } else {
                    $verif =
                        '&nbsp;<a href="javascript:void(0);" onclick="verif(\'' . $id . '\',\'' . $row->dt_status . '\')" class="btn btn-success btn-sm" title="Verifikasi">
                            <i class="fa fa-check"></i>
                        </a>';
                    $dt_status = '<span class="badge badge-success">Disetujui</span>';
                }

                if (empty($row->verif_by)) {
                    $action =
                        '<a href="javascript:void(0);" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit">
                            <i class="fa fa-pencil-alt"></i>
                        </a>' .
                        $verif .
                        '&nbsp;<a href="javascript:;" onclick="set_del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus">
                            <i class="fa fa-trash"></i>
                        </a>';
                }
                array_push(
                    $records["data"][$i],
                    "$row->pegawai_gd $row->pegawai_nama, $row->pegawai_gb<br>$dtd_status"
                );
                array_push(
                    $records["data"][$i],
                    $dt_status
                );
                array_push(
                    $records["data"][$i],
                    $action
                );
                $no++;
                $i++;
            }
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = [];
        $act = $this->input->post('act', true);
        $detail = $this->input->post('detail');

        $data = [
            "dt_judul" => $this->input->post("dt_judul"),
        ];

        if ($act == "add") {
            $data['dt_status'] = 0;
            $data['jurusan_id'] = $this->input->post("jurusan_id");
            $data['mhs_id'] = $this->input->post("mhs_id");
            $res = $this->data_ta_m->add($data);
        } else {
            if ($detail[0]['dtd_status'] == $detail[1]['dtd_status']) {
                $data['dt_status'] = $detail[0]['dtd_status'];
            } else {
                $data['dt_status'] = 0;
            }
            $id = $this->input->post('dt_id');
            $res = $this->data_ta_m->update($data, $id);
        }


        if ($res['status'] == 'true') {
            foreach ($detail as $key => $value) {
                $detail[$key]['dt_id'] = $res['id'];
                $detail[$key]['dtd_dospem_ke'] = $key + 1;
            }

            if ($act == "add") {
                $respond = $this->data_ta_m->insert_batch($detail);
            } else {
                $respond = $this->data_ta_m->update_batch($detail);
            }
        }

        if ($res['status'] == 'true' && $respond && count($_FILES) > 0) {
            $config['upload_path']      = $this->path;
            $config['allowed_types']    = 'pdf|doc';
            $config['max_size']         = '4096';
            $config['file_name']        = $res['id'];
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('dt_sinopsis')) {
                $data = [
                    'dt_sinopsis_path' => $this->upload->data('file_name'),
                    'dt_sinopsis_name' => $_FILES['dt_sinopsis']['name'],
                ];
                if ($this->data_ta_m->update($data, $res['id'])['status'] == 'true') {
                    $res = [
                        'status' => 'true',
                        'id' => $res['id'],
                    ];
                } else {
                    $res = [
                        'status' => 'false',
                        'msg' => 'Gagal update File',
                    ];
                }
            } else {
                $res = [
                    'status' => 'false',
                    'msg' =>  $this->upload->display_errors(),
                ];
            }
        }

        echo json_encode($res);
    }


    // 
    public function edit($id)
    {
        $data['data'] = $this->data_ta_m->edit($id);
        echo json_encode($data);
    }


    // delete
    public function hapus($id = 0)
    {
        $res['status'] = "false";
        if ($id != 0) {
            $file = $this->db->get_where('data_ta', ['dt_id' => $id])->row()->dt_sinopsis_path;
            if (!empty($file) && file_exists($this->path . '/' . $file)) {
                unlink(realpath($this->path . '/' . $file));
            }
            $res = $this->data_ta_m->delete($id);
        } else {
            $res['status'] = "false";
        }

        echo json_encode($res);
    }


    // cek jurusan_id
    public function cek_data($jenis)
    {
        $res = "";
        $where = "";
        $act = $this->input->post("act");
        $input = $input_lama = "";

        if ($jenis == 'jurusan_id') {
            $input = $this->input->post('jurusan_id');
            $input_lama = $this->input->post('jurusan_id_lama');
        } else if ($jenis == 'mhs_id') {
            $input = $this->input->post('mhs_id');
            $input_lama = $this->input->post('mhs_id_lama');
        }

        $where .= " AND $jenis = '$input'";

        if ($act == "edit") {
            $where .= " AND $jenis != '$input_lama'";
        }

        if ($this->data_ta_m->cek_data($where) > 0) {
            switch ($jenis) {
                case 'jurusan_id':
                    $res = "Kode sudah digunakan!";
                    break;
                case 'mhs_id':
                    $res = "Nama sudah digunakan!";
                    break;
            }
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }


    // mengambil data mahasiswa yang belum terdaftar ta
    public function get_mahasiswa($jurusan_id)
    {
        $data = $this->data_ta_m->get_mahasiswa($jurusan_id);
        echo json_encode($data);
    }
}
