<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/menu_m');
    }


    public function index()
    {
        $data['title'] = "Master Menu";
        $data['menu_id'] = "11";
        $data['parent'] = $this->menu_m->get_parent();
        $this->my_theme('admin/menu_v', $data);
    }


    // get data
    public function get_data()
    {
        $columns = [
            "mm.menu_id",
            "mm.menu_code",
            "mm.menu_name",
            "mm.menu_status",
            "mm.menu_url",
            "mm.menu_icon",
            "parent.menu_name as parent_name",
            "child.total_child",
        ];
        $columns_search = [
            "mm.menu_code",
            "mm.menu_name",
            "mm.menu_url",
            "mm.menu_icon",
            "parent_name",
        ];
        $columns_order = [
            "mm.menu_id",
            "mm.menu_code",
            "mm.menu_name",
            "mm.menu_status",
            "mm.menu_url",
            "mm.menu_icon",
            "parent_name",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->menu_m->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns_order[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns_order[$col] . " " . $dir;
        }

        $data = $this->menu_m->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $id = $row->menu_id;

            if ($row->menu_status == 1) {
                $menu_status = "<span class='badge badge-success'>Aktif</span>";
            } else {
                $menu_status = "<span class='badge badge-danger'>Non Aktif</span>";
            }
            $action = "<a class='btn btn-xs btn-warning' href='javascript:void(0);' title='edit' onclick='set_val($id)'>
                        <i class='fa fa-pencil-alt'></i>
                    </a>";
            if ($row->total_child <= 0) {
                $action .= " <a class='btn btn-xs btn-danger' href='javascript:void(0);' title='hapus' onclick='set_del($id)'>
                            <i class='fa fa-trash'></i>
                        </a>";
            }
            $records["data"][] = array(
                $no++,
                $row->menu_code,
                $row->menu_name,
                $row->menu_url,
                $row->menu_icon,
                $row->parent_name,
                $menu_status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // delete
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "menu_code" => $this->input->post("menu_code"),
            "menu_name" => $this->input->post("menu_name"),
            "menu_url" => $this->input->post("menu_url"),
            "parent_id" => $this->input->post("parent_id"),
            "parent_code" => $this->input->post("parent_code"),
            "menu_level" => $this->input->post("menu_level"),
            "jenis_menu" => $this->input->post("jenis_menu"),
            "menu_icon" => $this->input->post("menu_icon"),
            "menu_status" => $this->input->post("menu_status"),
        ];

        if ($act == "add") {
            $res = $this->menu_m->add($data);
        } else {
            $id = $this->input->post('menu_id');
            $res = $this->menu_m->update($data, $id);
        }

        echo json_encode($res);
    }


    // 
    public function edit($id)
    {
        $data['data'] = $this->menu_m->edit($id);
        echo json_encode($data);
    }


    // delete
    public function hapus($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->menu_m->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek menu_code
    public function cek_data($jenis)
    {
        $res = "";
        $where = "";
        $act = $this->input->post("act");
        $input = $input_lama = "";

        if ($jenis == 'menu_code') {
            $input = $this->input->post('menu_code');
            $input_lama = $this->input->post('menu_code_lama');
        }

        $where .= " AND $jenis = '$input'";

        if ($act == "edit") {
            $where .= " AND $jenis != '$input_lama'";
        }

        if ($this->menu_m->cek_data($where) > 0) {
            switch ($jenis) {
                case 'menu_code':
                    $res = "Kode sudah digunakan!";
                    break;
            }
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }


    // 
    public function get_parent()
    {
        $data = $this->menu_m->get_parent();
        echo json_encode($data);
    }
}
