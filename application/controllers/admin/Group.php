<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Group extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/group_m');
    }


    public function index()
    {
        $data['title'] = "Master Group";
        $data['menu_id'] = "10";
        $this->my_theme('admin/group_v', $data);
    }


    // get data
    public function get_data()
    {
        $columns = [
            "mg.group_id",
            "mg.group_code",
            "mg.group_name",
            "mg.group_status",
        ];
        $columns_search = [
            "mg.group_code",
            "mg.group_name",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->group_m->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->group_m->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            if ($row->group_status == 1) {
                $group_status = "<span class='badge badge-success'>Aktif</span>";
            } else {
                $group_status = "<span class='badge badge-danger'>Non Aktif</span>";
            }
            $id = $row->group_id;
            $akses = "<a class='btn btn-xs btn-warning' href='javascript:void(0);' title='Setting Hak Akses' onclick='set_akses($id,\"$row->group_name\")'>
                        <i class='fas fa-fw fa-cog'></i>
                    </a>";
            $action = "<a class='btn btn-xs btn-info' href='javascript:void(0);' title='edit' onclick='set_val($id)'>
                        <i class='fa fa-pencil-alt'></i>
                    </a>
                    <a class='btn btn-xs btn-danger' href='javascript:void(0);' title='hapus' onclick='set_del($id)'>
                        <i class='fa fa-trash'></i>
                    </a>";
            $records["data"][] = array(
                $no++,
                $row->group_code,
                $row->group_name,
                $group_status,
                $akses,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // delete
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "group_code" => $this->input->post("group_code"),
            "group_name" => $this->input->post("group_name"),
            "group_status" => $this->input->post("group_status"),
        ];

        if ($act == "add") {
            $res = $this->group_m->add($data);
        } else {
            $id = $this->input->post('group_id');
            $res = $this->group_m->update($data, $id);
        }

        echo json_encode($res);
    }


    // 
    public function edit($id)
    {
        $data['data'] = $this->group_m->edit($id);
        echo json_encode($data);
    }


    // delete
    public function hapus($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->group_m->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek group_code
    public function cek_data($jenis)
    {
        $res = "";
        $where = "";
        $act = $this->input->post("act");
        $input = $input_lama = "";

        if ($jenis == 'group_code') {
            $input = $this->input->post('group_code');
            $input_lama = $this->input->post('group_code_lama');
        } else if ($jenis == 'group_name') {
            $input = $this->input->post('group_name');
            $input_lama = $this->input->post('group_name_lama');
        }

        $where .= " AND $jenis = '$input'";

        if ($act == "edit") {
            $where .= " AND $jenis != '$input_lama'";
        }

        if ($this->group_m->cek_data($where) > 0) {
            switch ($jenis) {
                case 'group_code':
                    $res = "Kode sudah digunakan!";
                    break;
                case 'group_name':
                    $res = "Nama sudah digunakan!";
                    break;
            }
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }


    // ambil data menu untuk dijadikan tree
    public function get_tree_menu()
    {
        $id = $this->input->get('group_id');
        if (empty($id)) {
            $id = 0;
        }
        $res = $this->group_m->get_tree_menu($id);
        $data = [];
        foreach ($res as $key => $value) {
            $data[] = [
                'id' => $value->menu_id,
                'parent' => $value->parent_id == '0' ? '#' : $value->parent_id,
                'text' => "$value->menu_name",
                'state' => [
                    'opened' => true
                ],
                'li_attr' => $value,
                'a_attr' => $value,
            ];
        }

        echo json_encode($data);
    }


    // fungsi mengambil menu yang belum terinsert dalam group tersebut
    public function get_menu()
    {
        $group_id = $this->input->get('group_id');
        $res = $this->group_m->get_menu($group_id);
        echo json_encode($res);
    }


    // fungsi tambah akses
    public function tambah_akses()
    {
        $group_id = $this->input->post('group_id');
        $menu_id = $this->input->post('menu_id');
        $data = [
            'group_id' => $group_id,
            'menu_id' => $menu_id
        ];
        $res = $this->group_m->tambah_akses($data);
        echo json_encode($res);
    }


    // fungsi hapus akses
    public function hapus_akses()
    {
        $group_id = $this->input->post('group_id');
        $menu_id = $this->input->post('menu_id');
        $data = [
            'group_id' => $group_id,
            'menu_id' => $menu_id
        ];
        $res = $this->group_m->hapus_akses($data);
        echo json_encode($res);
    }
}
