<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/user_m');
    }


    public function index()
    {
        $data['title'] = "Master User";
        $data['jurusan'] = $this->db
            ->where('jurusan_status', 1)
            ->order_by('jurusan_code', 'asc')
            ->get('jurusan')
            ->result();
        $data['menu_id'] = "7";
        $this->my_theme('admin/user_v', $data);
    }


    // get data
    public function get_data()
    {
        $f_jenis_profil = $this->input->post('f_jenis_profil');
        $user_fullname = $f_jenis_profil == 2 ? "p.pegawai_nama as user_fullname" : "m.mhs_name as user_fullname";
        $field_fullname = $f_jenis_profil == 2 ? "p.pegawai_nama" : "m.mhs_name";
        $columns = [
            "mu.user_id",
            "mu.username",
            "$user_fullname",
            "mu.user_status",
            "mu.jenis_profil",
        ];
        $columns_search = [
            "mu.username",
            "$field_fullname",
        ];
        $columns_order = [
            "mu.user_id",
            "mu.username",
            "$field_fullname",
            "mu.user_status",
            "mu.jenis_profil",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->user_m->get_total($where, $f_jenis_profil);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns_order[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns_order[$col] . " " . $dir;
        }

        $data = $this->user_m->get_data($columns, $where, $order, $limit, $f_jenis_profil);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->user_id;

            if ($row->user_status == 1) {
                $user_status = "<span class='badge badge-success'>Aktif</span>";
            } else {
                $user_status = "<span class='badge badge-danger'>Non Aktif</span>";
            }
            $akses = '<a href="javascript:void(0);" onclick="set_akses(\'' . $id . '\',\'' . $row->username . '\')" class="btn btn-warning btn-sm" title="Edit">
                            <i class="fa fa-cog"></i>
                        </a>';

            $action = '<a href="javascript:void(0);" onclick="set_val(\'' . $id . '\',\'' . $row->jenis_profil . '\')" class="btn btn-info btn-sm" title="Edit">
                            <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a href="javascript:;" onclick="set_del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus">
                            <i class="fa fa-trash"></i>
                        </a>';
            $records["data"][] = array(
                $no++,
                $row->username,
                $row->user_fullname,
                $user_status,
                $akses,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // delete
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "username" => $this->input->post("username"),
            "profil_id" => $this->input->post("profil_id"),
            "user_status" => $this->input->post("user_status"),
            "password" => password_hash($this->input->post("password"), PASSWORD_DEFAULT),
            "jenis_profil" => $this->input->post("jenis_profil"),
        ];

        if ($act == "add") {
            $res = $this->user_m->add($data);
        } else {
            if ($this->input->post("gantiPassword") != "on") {
                unset($data["password"]);
            }
            $id = $this->input->post('user_id');
            $res = $this->user_m->update($data, $id);
        }

        echo json_encode($res);
    }


    // 
    public function edit()
    {
        $id = $this->input->get('id');
        $jenis_profil = $this->input->get('jenis_profil');
        $data['data'] = $this->user_m->edit($id, $jenis_profil);
        echo json_encode($data);
    }


    // delete
    public function hapus($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->user_m->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek username
    public function cek_data($jenis)
    {
        $res = "";
        $where = "";
        $act = $this->input->post("act");
        $input = $input_lama = "";

        if ($jenis == 'username') {
            $input = $this->input->post('username');
            $input_lama = $this->input->post('username_lama');
        }

        $where .= " AND $jenis = '$input'";

        if ($act == "edit") {
            $where .= " AND $jenis != '$input_lama'";
        }

        if ($this->user_m->cek_data($where) > 0) {
            $res = "Username sudah ada";
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }


    // ambil data menu untuk dijadikan tree
    public function list_group()
    {
        $id = $this->input->get('id');
        if (empty($id)) {
            $id = 0;
        }
        $res = $this->user_m->list_group($id);

        echo json_encode($res);
    }


    // fungsi tambah akses
    public function simpan_akses()
    {
        $group_id = $this->input->post('group_id');
        $user_id = $this->input->post('user_id');
        foreach ($group_id as $value) {
            $data[] = [
                'group_id' => $value,
                'user_id' => $user_id
            ];
        }
        $res = $this->user_m->simpan_akses($data, $user_id);
        echo json_encode($res);
    }


    // fungsi tambah akses
    public function get_profil()
    {
        $search = $this->input->get('search');
        $jurusan_id = $this->input->get('jurusan_id');
        $jenis_profil = $this->input->get('jenis_profil');

        $data = [
            'search' => $search,
            'jenis_profil' => $jenis_profil,
            'jurusan_id' => $jurusan_id,
        ];

        $res = $this->user_m->get_profil($data);
        echo json_encode($res);
    }
}
