<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calon extends MY_Controller
{
    public $path = 'assets/global/tugas_akhir/daftar';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dosen/calon_m');
    }


    public function index()
    {
        $data['jurusan'] = $this->db->where('jurusan_status', 1)->order_by('jurusan_code', 'asc')->get('jurusan')->result();
        $this->load->view('dosen/calon_v', $data);
    }


    // get data krisis
    public function get_data()
    {
        $columns = [
            "dt.dt_id",
            "dt.dt_judul",
            "dt.dt_sinopsis_name",
            "dt.dt_sinopsis_path",
            "concat(m.mhs_nim, ' - ', m.mhs_name) as mhs_name",
            "dtd.dtd_id",
            "dtd.dtd_status",
        ];

        $columns_search = [
            "dt.dt_id",
            "mhs_name",
            "dt.dt_judul",
            "dtd.dtd_id",
            "dt.dt_sinopsis_name",
            "dt.dt_sinopsis_path",
        ];

        $draw = intval($this->input->post("draw"));
        $f_jurusan_id = $this->input->post("f_jurusan_id");
        $f_status = $this->input->post("f_status");

        $where     = " AND dtd.dosen_id = " . $this->userData['profil_id'];
        $where     .= " AND dtd.dtd_status = $f_status";
        $where     .= " AND dt.jurusan_id = $f_jurusan_id";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                if ($i == 1 || $i == 2) {
                    $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
                }
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->calon_m->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->calon_m->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = $file = "";
            $id = $row->dtd_id;
            $data_lain = $this->db->select('dtd_status')->from('data_ta_detail')->where('dt_id', $row->dt_id)->where('dtd_id !=', $id)->get()->row()->dtd_status;

            if ($row->dtd_status == 0) {
                $action = '<a href="javascript:void(0);" onclick="dSetVal(\'' . $id . '|2|' . $data_lain . '|' . $row->dt_id . '\')" class="btn btn-success btn-sm" title="Setuju">
                                <i class="fa fa-check"></i>
                            </a>
                            <a href="javascript:;" onclick="dSetVal(\'' . $id . '|1|' . $data_lain . '|' . $row->dt_id . '\')" class="btn btn-danger btn-sm" title="Tolak">
                                <i class="fa fa-times"></i>
                            </a>';
            }
            if ($row->dtd_status != 1) {
                $file = '<a href="' . base_url() . $this->path . '/' . $row->dt_sinopsis_path . '" download="' . addslashes($row->dt_sinopsis_name) . '" target="_blank" class="btn btn-info btn-sm" title="Sinopsis">
                                <i class="fa fa-file"></i>
                            </a>';
            }
            $records["data"][] = array(
                $no++,
                $row->mhs_name,
                $row->dt_judul,
                $file,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $data = [
            "dtd_id" => $this->input->post("id"),
            "dtd_status" => $this->input->post("status"),
        ];

        $res = $this->calon_m->update('data_ta_detail', $data, 'dtd_id');

        if (!empty($this->input->post('parent'))) {
            $res = $this->calon_m->update('data_ta', $this->input->post('parent'), 'dt_id');
        }

        echo json_encode($res);
    }
}
