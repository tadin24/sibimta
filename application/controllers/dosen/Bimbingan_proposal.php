<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/jurusan_m');
    }


    public function index()
    {
        $data['title'] = "Master Jurusan";
        $data['menu_id'] = "3";
        $this->my_theme('admin/jurusan_v', $data);
    }


    // get data krisis
    public function get_data()
    {
        $columns = [
            "j.jurusan_id",
            "j.jurusan_code",
            "j.jurusan_name",
            "j.jurusan_status",

        ];
        $columns_search = [
            "j.jurusan_code",
            "j.jurusan_name",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->jurusan_m->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->jurusan_m->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->jurusan_id;

            if ($row->jurusan_status == 1) {
                $jurusan_status = '<span class="badge badge-success">Aktif</span>';
            } else {
                $jurusan_status = '<span class="badge badge-danger">Non Aktif</span>';
            }

            $action = '<a href="javascript:void(0);" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit">
                            <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a href="javascript:;" onclick="set_del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus">
                            <i class="fa fa-trash"></i>
                        </a>';
            $records["data"][] = array(
                $no++,
                $row->jurusan_code,
                $row->jurusan_name,
                $jurusan_status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "jurusan_code" => $this->input->post("jurusan_code"),
            "jurusan_name" => $this->input->post("jurusan_name"),
            "jurusan_status" => $this->input->post("jurusan_status"),
        ];

        if ($act == "add") {
            $res = $this->jurusan_m->add($data);
        } else {
            $id = $this->input->post('jurusan_id');
            $res = $this->jurusan_m->update($data, $id);
        }

        echo json_encode($res);
    }


    // 
    public function edit($id)
    {
        $data['data'] = $this->jurusan_m->edit($id);
        echo json_encode($data);
    }


    // delete
    public function hapus($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->jurusan_m->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek jurusan_code
    public function cek_data($jenis)
    {
        $res = "";
        $where = "";
        $act = $this->input->post("act");
        $input = $input_lama = "";

        if ($jenis == 'jurusan_code') {
            $input = $this->input->post('jurusan_code');
            $input_lama = $this->input->post('jurusan_code_lama');
        } else if ($jenis == 'jurusan_name') {
            $input = $this->input->post('jurusan_name');
            $input_lama = $this->input->post('jurusan_name_lama');
        }

        $where .= " AND $jenis = '$input'";

        if ($act == "edit") {
            $where .= " AND $jenis != '$input_lama'";
        }

        if ($this->jurusan_m->cek_data($where) > 0) {
            switch ($jenis) {
                case 'jurusan_code':
                    $res = "Kode sudah digunakan!";
                    break;
                case 'jurusan_name':
                    $res = "Nama sudah digunakan!";
                    break;
            }
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }
}
