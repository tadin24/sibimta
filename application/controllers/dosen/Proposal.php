<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proposal extends MY_Controller
{
    public $path = './assets/global/tugas_akhir/daftar';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dosen/proposal_m');
    }


    public function index()
    {
        $data['title'] = "Pengajuan Proposal Tugas Akhir";
        $data['menu_id'] = "26";
        $data['submenu'] = $this->db->where('jenis_menu', 3)->where('menu_status', 1)->where('parent_id', 26)->order_by('menu_code', 'asc')->get('ms_menu')->result();
        $this->my_theme('dosen/proposal_v', $data);
    }


    // save
    public function save()
    {
        $res = [];
        $dt_id = $this->input->post('dt_id', true);

        $data = [
            "mhs_id" => $this->input->post("d_mhs_id"),
            "jurusan_id" => $this->input->post("d_jurusan_id"),
            "dt_judul" => $this->input->post("dt_judul"),
        ];

        if (array_key_exists('dt_sinopsis', $_FILES)) {
            $data["dt_sinopsis_name"] = $_FILES["dt_sinopsis"]["name"];
        }

        if (empty($dt_id)) {
            $dt_id = $this->proposal_m->add($data)['id'];
        } else {
            $this->proposal_m->update($data, $dt_id);
        }

        $insert = 0;

        foreach ($this->input->post('detail') as $key => $value) {
            $data_detail[$key] = [
                "dosen_id" => $value['dosen_id'],
            ];

            if ($value['dtd_id'] != '') {
                $data_detail[$key]['dtd_id'] = $value['dtd_id'];
            } else {
                $data_detail[$key]['dt_id'] = $dt_id;
                $data_detail[$key]['dtd_dospem_ke'] =  $key + 1;
                $data_detail[$key]['dtd_status'] = 0;
                $insert++;
            }
        }

        if ($insert > 0) {
            $res = $this->proposal_m->add($data_detail, 'detail');
        } else {
            $res = $this->proposal_m->update($data_detail, 'dtd_id', 'detail');
        }

        // if ($this->input->post('is_delete') == 'yes' && $res['status'] == 'true') {
        //     $image = $this->db->get_where('dosen', ['mhs_id' => $res['id']])->row()->photo;
        //     if (!empty($image) && file_exists(realpath($this->path . '/' . $image))) {
        //         unlink(realpath($this->path . '/' . $image));
        //     }
        //     $data = [
        //         'photo' => null,
        //     ];
        //     $res = $this->proposal_m->update($data, $res['id']);
        // }

        if (count($_FILES) > 0 && $dt_id > 0) {
            $config['upload_path']      = $this->path;
            $config['allowed_types']    = 'pdf|doc|docx';
            $config['max_size']         = '5120';
            $config['file_name']        = $dt_id;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('dt_sinopsis')) {
                $data = [
                    'dt_sinopsis_path' => $this->upload->data('file_name')
                ];
                if ($this->proposal_m->update($data, $dt_id)['status'] == 'true') {
                    $res = [
                        'status' => 'true',
                        'id' => $dt_id,
                    ];
                } else {
                    $res = [
                        'status' => 'false',
                        'msg' => 'Gagal update foto',
                    ];
                }
            } else {
                $res = [
                    'status' => 'false',
                    'msg' =>  $this->upload->display_errors(),
                ];
            }
        }

        echo json_encode($res);
    }


    // delete
    public function hapus($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->proposal_m->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }
}
