<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Global_m extends CI_Model
{
    // mengambil submenu
    public function get_submenu($parent)
    {
        $sql = "SELECT
                    *
                from
                    ms_menu mm
                where
                    mm.parent_id = $parent
                    and mm.jenis_menu = 3
                    and mm.menu_status = 1
                order by
                    mm.menu_code";
        return $this->db->query($sql)->result();
    }
}
