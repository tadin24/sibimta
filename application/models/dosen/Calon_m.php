<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calon_m extends CI_Model
{
    public $table = "jurusan";
    public $id = "jurusan_id";


    // get total
    public function get_total($where)
    {
        return $this->db->query(
            "SELECT
                count(*) as total
            from
                data_ta dt
            inner join data_ta_detail dtd on
                dt.dt_id = dtd.dt_id
            inner join 
                mahasiswa m on
                m.mhs_id = dt.mhs_id
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            from
                data_ta dt
            inner join data_ta_detail dtd on
                dt.dt_id = dtd.dt_id
            inner join 
                mahasiswa m on
                m.mhs_id = dt.mhs_id
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // update
    public function update($table, $data, $param)
    {
        $res = null;
        $this->db->where($param, $data[$param]);
        $result = $this->db->update($table, $data);
        if ($result) {
            $res['status'] = "true";
        } else {
            $res['status'] = "false";
        }
        return $res;
    }
}
