<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calon_m extends CI_Model
{
    public $table = "jurusan";
    public $id = "jurusan_id";


    // get total
    public function get_total($where)
    {
        return $this->db->query(
            "SELECT
                count(*) as total
            FROM
                $this->table j
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            FROM
                $this->table j
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // edit
    public function edit($id)
    {
        $res = [];
        $this->db->select('jurusan_id,jurusan_code,jurusan_name,jurusan_status');
        $this->db->from($this->table);
        $this->db->where($this->id, $id);
        $res = $this->db->get()->row();
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $result = $this->db->update($this->table, $data);
        if ($result) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek jurusan_code
    public function cek_data($where)
    {
        $sql = "SELECT * from $this->table where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }
}
