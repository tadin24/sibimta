<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data_ta_m extends CI_Model
{
    public $table = 'data_ta';
    public $id = 'dt_id';
    // get total
    public function get_total($where)
    {
        return $this->db->query(
            "SELECT
                count(distinct dt.dt_id) as total
            from
                data_ta dt
            inner join data_ta_detail dtd on
                dt.dt_id = dtd.dt_id
            inner join mahasiswa m on
                m.mhs_id = dt.mhs_id
            inner join pegawai p on
                p.pegawai_id = dtd.dosen_id
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            from
                data_ta dt
            inner join data_ta_detail dtd on
                dt.dt_id = dtd.dt_id
            inner join mahasiswa m on
                m.mhs_id = dt.mhs_id
            inner join pegawai p on
                p.pegawai_id = dtd.dosen_id
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // add
    public function add($data)
    {
        $res['status'] = "false";
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            $res['status'] = "true";
            $res['id'] = $this->db->insert_id();
        } else {
            $res['status'] = "false";
            $res['msg'] = "Gagal insert data";
        }
        return $res;
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->query(
            "SELECT
                dt.dt_id ,
                dt.dt_judul ,
                dt.dt_sinopsis_name ,
                concat(m.mhs_nim , ' - ', m.mhs_name) as mhs_name,
                j.jurusan_name
            from
                data_ta dt
            left join mahasiswa m on
                m.mhs_id = dt.mhs_id
            left join jurusan j on
                j.jurusan_id = dt.jurusan_id
            where
                dt.dt_id = $id"
        )->row();
        $res->detail = $this->db->query(
            "SELECT
                *
            from
                data_ta_detail dtd
            where
                dtd.dt_id = $id
            order by
                dtd.dtd_dospem_ke"
        )->result();
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res['status'] = "false";
        $this->db->where($this->id, $id);
        $result = $this->db->update($this->table, $data);
        if ($result) {
            $res['status'] = "true";
            $res['id'] = $id;
        } else {
            $res['status'] = "false";
            $res['msg'] = "Gagal update data";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res['status'] = "false";
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            $res['status'] = "true";
        } else {
            $res['status'] = "false";
        }
        return $res;
    }


    // cek jurusan_code
    public function cek_data($where)
    {
        $sql = "SELECT * from $this->table where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }


    // get dosen
    public function get_dosen()
    {
        return $this->db->query(
            "SELECT
                p.pegawai_id ,
                concat(p.pegawai_gd, ' ', p.pegawai_nama, ', ', p.pegawai_gb) as pegawai_nama
            from
                pegawai p
            inner join kategori_pegawai kp on
                kp.kp_id = p.kp_id
            where
                kp.kp_code like '01%'
                and pegawai_status = 1
            order by
                pegawai_nama;"
        )->result();
    }


    // get mahasiswa
    public function get_mahasiswa($jurusan_id)
    {
        $sql = "SELECT
                    m.mhs_id ,
                    concat(m.mhs_nim ,' - ', m.mhs_name) as mhs_name
                FROM
                    mahasiswa m
                where
                    m.mhs_status = 1
                    and m.jurusan_id = $jurusan_id
                    and m.mhs_id not in (
                    SELECT
                        d.mhs_id
                    FROM
                        data_ta d)
                order by
                    m.mhs_nim";
        $res = $this->db->query($sql)->result();
        return $res;
    }


    // insert bacth
    public function insert_batch($data)
    {
        $this->db->insert_batch('data_ta_detail', $data);
        return true;
    }


    // update bacth
    public function update_batch($data)
    {
        $this->db->update_batch('data_ta_detail', $data, 'dtd_id');
        return true;
    }
}
