<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Group_m extends CI_Model
{
    public $table = "ms_group";
    public $id = "group_id";


    // get total
    public function get_total($where)
    {
        return $this->db->query(
            "SELECT
                count(*) as total
            FROM
                $this->table mg
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            FROM
                $this->table mg
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // edit
    public function edit($id)
    {
        $res = [];
        $this->db->select('group_id,group_code,group_name,group_status');
        $this->db->from($this->table);
        $this->db->where($this->id, $id);
        $res = $this->db->get()->row();
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $result = $this->db->update($this->table, $data);
        if ($result) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek group_code
    public function cek_data($where)
    {
        $sql = "SELECT * from $this->table where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }


    // get tree menu
    public function get_tree_menu($id)
    {
        $sql = "SELECT
                    mm.menu_id ,
                    mm.menu_code ,
                    mm.menu_name ,
                    mm.parent_id
                from
                    group_menu gm
                inner join ms_menu mm on
                    mm.menu_id = gm.menu_id
                where
                    gm.group_id = $id
                order BY
                    mm.menu_code";
        $res = $this->db->query($sql)->result();

        return $res;
    }


    // get tree menu
    public function get_menu($group_id)
    {
        $sql = "SELECT
                    mm.menu_id ,
                    mm.menu_code ,
                    mm.menu_name
                from
                    ms_menu mm
                where
                    mm.menu_status = 1
                    and mm.menu_id not in (
                    SELECT
                        gm.menu_id 
                    from
                        group_menu gm
                    where
                        gm.group_id = $group_id)
                order BY
                    mm.menu_code";
        $res = $this->db->query($sql)->result();

        return $res;
    }


    // tambah akses
    public function tambah_akses($data)
    {
        $res = "";
        $this->db->insert('group_menu', $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // hapus akses
    public function hapus_akses($data)
    {
        $res = "";
        $this->db->where('group_id', $data['group_id']);
        $this->db->where_in('menu_id', $data['menu_id']);
        $this->db->delete('group_menu');
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }
}
