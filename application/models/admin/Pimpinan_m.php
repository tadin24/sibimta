<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pimpinan_m extends CI_Model
{
    public $table = 'group_user';
    public $group_id = 5;


    // get total
    public function get_total($where)
    {
        return $this->db->query(
            "SELECT
                count(*) as total
            from
                $this->table gu
            inner join ms_user mu on
                mu.user_id = gu.user_id
                and mu.jenis_profil = 2
                and gu.group_id = $this->group_id
            inner join pegawai p on
                p.pegawai_id = mu.profil_id
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            from
                $this->table gu
            inner join ms_user mu on
                mu.user_id = gu.user_id
                and mu.jenis_profil = 2
                and gu.group_id = $this->group_id
            inner join pegawai p on
                p.pegawai_id = mu.profil_id
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id, $group_id = 0)
    {
        $res = "";
        $this->db->where('user_id', $id);
        $this->db->where('group_id', $group_id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows()) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // get_pegawai
    public function get_pegawai()
    {
        $sql = "SELECT
                    mu.user_id,
                    concat(p.pegawai_gd , ' ', p.pegawai_nama , ' ', p.pegawai_gb, '(', p.pegawai_nip, ')') as pegawai_nama
                from
                    ms_user mu
                inner join pegawai p on
                    p.pegawai_id = mu.profil_id
                    and mu.jenis_profil = 2
                where
                    mu.user_status = 1
                    and mu.user_id not in (
                    select
                        gu.user_id
                    from
                        group_user gu
                    where
                        gu.group_id = $this->group_id)";
        $res = $this->db->query($sql)->result();
        return $res;
    }
}
