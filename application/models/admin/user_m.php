<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_m extends CI_Model
{
    public $table = "ms_user";
    public $id = "user_id";


    // get total
    public function get_total($where, $jenis_profil = 2)
    {
        if ($jenis_profil == 2) {
            $join = "inner join pegawai p on
            p.pegawai_id = mu.profil_id";
        } else {
            $join = "inner join mahasiswa m on
            m.mhs_id = mu.profil_id
            ";
        }
        $join .= " AND mu.jenis_profil = $jenis_profil";
        return $this->db->query(
            "SELECT
                count(*) as total
            FROM
                $this->table mu
                $join
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit, $jenis_profil = 2)
    {
        if ($jenis_profil == 2) {
            $join = "inner join pegawai p on
            p.pegawai_id = mu.profil_id";
        } else {
            $join = "inner join mahasiswa m on
            m.mhs_id = mu.profil_id
            ";
        }
        $join .= " AND mu.jenis_profil = $jenis_profil";
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            FROM
                $this->table mu
                $join
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // edit
    public function edit($id, $jenis_profil)
    {
        if ($jenis_profil == 2) {
            $selector = "p.pegawai_nama as user_fullname";
            $join = "inner join pegawai p on
            p.pegawai_id = mu.profil_id";
        } else {
            $selector = "m.mhs_name as user_fullname,m.jurusan_id";
            $join = "inner join mahasiswa m on
            m.mhs_id = mu.profil_id
            ";
        }
        $join .= " AND mu.jenis_profil = $jenis_profil";
        $sql = "SELECT
                    mu.*,
                    $selector
                FROM
                    $this->table mu
                    $join
                where
                    mu.user_id = $id
                ORDER BY
                    mu.username";
        $res = $this->db->query($sql)->row();
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $result = $this->db->update($this->table, $data);
        if ($result) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek username
    public function cek_data($where)
    {
        $sql = "SELECT * from $this->table where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }


    // get tree menu
    public function list_group($id)
    {
        $sql = "SELECT
                    mg.*,
                    CASE
                        when gu.group_id is not null then 1
                        else 0
                    end as checked
                FROM
                    ms_group mg
                left join group_user gu on
                    gu.group_id = mg.group_id
                    and gu.user_id = $id
                WHERE
                    mg.group_status = 1
                order by
                    mg.group_code";
        $res = $this->db->query($sql)->result();

        return $res;
    }


    // simpan akses
    public function simpan_akses($data, $user_id)
    {
        $res = "";
        $this->db->where('user_id', $user_id);
        $this->db->delete('group_user');
        $this->db->insert_batch('group_user', $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // get profil
    public function get_profil($data)
    {
        $search = $data['search'];
        $jenis_profil = $data['jenis_profil'];
        if ($jenis_profil == 1) {
            $jurusan_id = $data['jurusan_id'];
            $sql = "SELECT
                        m.mhs_id as id,
                        concat(m.mhs_name, '(', m.mhs_nim, ')') as text
                    from
                        mahasiswa m
                    where
                        m.jurusan_id = $jurusan_id
                        and m.mhs_id not in (
                        select
                            profil_id as mhs_id
                        from
                            ms_user mu
                        where
                            mu.jenis_profil = $jenis_profil)
                        and lower(m.mhs_name) like '%$search%'";
        } else {
            $sql = "SELECT
                        p.pegawai_id as id,
                        concat(p.pegawai_gd, ' ', p.pegawai_nama, ' ', p.pegawai_gb) as text
                    from
                        pegawai p
                    where
                        p.pegawai_status = 1
                        and p.pegawai_id not in (
                        select
                            mu.user_id
                        from
                            ms_user mu
                        where
                            mu.jenis_profil = 2)
                        and lower(p.pegawai_nama) like '%$search%'";
        }
        $res = $this->db->query($sql)->result();

        return $res;
    }
}
