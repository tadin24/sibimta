<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_in_group_m extends CI_Model
{
    // get total
    public function get_total($where, $param)
    {
        $where1 = $param['where1'];
        $where2 = $param['where2'];
        $not = $param['dari'] == 'kiri' ? 'not' : '';
        $sql = "SELECT
                    count(*) as total
                from
                    ms_user mu
                where
                    $where1
                    and mu.user_id $not in(
                    select
                        gu.user_id
                    from
                        group_user gu
                    where
                        $where2)
                    $where;
                ";
        return $this->db->query(
            $sql
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit, $param)
    {
        $selector = implode(',', $columns);
        $where1 = $param['where1'];
        $where2 = $param['where2'];
        $not = $param['dari'] == 'kiri' ? 'not' : '';
        $sql = "SELECT
                    $selector
                from
                    ms_user mu
                where
                    $where1
                    and mu.user_id $not in(
                    select
                        gu.user_id
                    from
                        group_user gu
                    where
                        $where2)
                    $where
                    $where
                $order $limit
                ";
        return $this->db->query($sql)->result();
    }


    // add
    public function add($data)
    {
        $res = null;
        $this->db->insert_batch('group_user', $data);
        if ($this->db->affected_rows()) {
            $res['status'] = "true";
        } else {
            $res['status'] = "false";
            $res['msg'] = "Gagal menambahkan";
        }
        return $res;
    }


    // delete
    public function delete($user_id, $group_id)
    {
        $res = null;
        $this->db->where_in('user_id', $user_id);
        $this->db->where('group_id', $group_id);
        $this->db->delete('group_user');
        if ($this->db->affected_rows()) {
            $res['status'] = "true";
        } else {
            $res['status'] = "false";
            $res['msg'] = "Gagal menghapus";
        }
        return $res;
    }


    // get group
    public function get_group()
    {
        $this->db->where('group_status', 1);
        $this->db->order_by('group_code', 'asc');
        $res = $this->db->get('ms_group')->result();
        return $res;
    }
}
