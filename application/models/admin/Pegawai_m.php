<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai_m extends CI_Model
{
    public $table = "pegawai";
    public $id = "pegawai_id";


    // get total
    public function get_total($where)
    {
        return $this->db->query(
            "SELECT
                count(*) as total
            FROM
                $this->table p
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            FROM
                $this->table p
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // add
    public function add($data)
    {
        $res['status'] = "false";
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            $res['status'] = "true";
            $res['id'] = $this->db->insert_id();
        } else {
            $res['status'] = "false";
            $res['msg'] = "Gagal Tambah Data";
        }
        return $res;
    }


    // edit
    public function edit($id)
    {
        $res = [];
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->id, $id);
        $res = $this->db->get()->row();
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res['status'] = "false";
        $this->db->where($this->id, $id);
        $result = $this->db->update($this->table, $data);
        if ($result) {
            $res['status'] = "true";
            $res['id'] = $id;
        } else {
            $res['status'] = "false";
            $res['msg'] = "Error Update";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek pegawai_nip
    public function cek_data($where)
    {
        $sql = "SELECT * from $this->table where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }
}
