<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_m extends CI_Model
{
    public $table = "ms_menu";
    public $id = "menu_id";


    // get total
    public function get_total($where)
    {
        return $this->db->query(
            "SELECT
                count(*) as total
            FROM
                $this->table mm
            left join $this->table parent on 
                parent.menu_id = mm.parent_id
            left join (
                select
                    count(*) as total_child,
                    mm2.parent_id
                from
                    $this->table mm2
                group by
                    mm2.parent_id
            ) as child on 
                child.parent_id = mm.menu_id
            WHERE
                0 = 0
                $where"
        )->row()->total;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(',', $columns);
        return $this->db->query(
            "SELECT
                $selector
            FROM
                $this->table mm
            left join $this->table parent on 
                parent.menu_id = mm.parent_id
            left join (
                select
                    count(*) as total_child,
                    mm2.parent_id
                from
                    $this->table mm2
                group by
                    mm2.parent_id
            ) as child on 
                child.parent_id = mm.menu_id
            WHERE
                0 = 0
                $where
            $order $limit"
        )->result();
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // edit
    public function edit($id)
    {
        $sql = "SELECT
                    mm.menu_id,
                    mm.menu_code,
                    mm.menu_name,
                    mm.menu_status,
                    mm.menu_url,
                    mm.menu_icon,
                    coalesce(parent.menu_name,'') as parent_label,
                    mm.parent_id,
                    mm.parent_code,
                    mm.menu_level,
                    mm.jenis_menu
                FROM
                    $this->table mm
                left join $this->table parent on 
                    parent.menu_id = mm.parent_id
                WHERE
                    mm.menu_id = $id";
        $res = $this->db->query($sql)->row();
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $result = $this->db->update($this->table, $data);
        if ($result) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek menu_code
    public function cek_data($where)
    {
        $sql = "SELECT * from $this->table where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }


    // get parent
    public function get_parent()
    {
        $sql = "SELECT * from $this->table order by menu_code";
        $res = $this->db->query($sql)->result();
        if ($res) {
            $res = $res;
        } else {
            $res = [];
        }

        return $res;
    }
}
