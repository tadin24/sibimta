<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendaftaran_m extends CI_Model
{
    public $table = "data_ta";
    public $id = "dt_id";


    // get total
    public function get_dosen()
    {
        return $this->db->query(
            "SELECT
                p.pegawai_id ,
                concat(p.pegawai_gd, ' ', p.pegawai_nama, ', ', p.pegawai_gb) as pegawai_nama
            from
                pegawai p
            inner join kategori_pegawai kp on
                kp.kp_id = p.kp_id
            where
                kp.kp_code like '01%'
                and pegawai_status = 1
            order by
                pegawai_nama;"
        )->result();
    }


    // get userdata
    public function get_userdata($profil_id)
    {
        return $this->db->query(
            "SELECT
                m.mhs_id ,
                m.mhs_name ,
                m.mhs_nim ,
                m.jurusan_id ,
                j.jurusan_name ,
                dt.dt_id ,
                dt.dt_judul ,
                dt.dt_sinopsis_name,
                dt.dt_sinopsis_path
            from
                mahasiswa m
            inner join jurusan j on
                j.jurusan_id = m.jurusan_id
            left join data_ta dt on
                dt.mhs_id = m.mhs_id
            where
                m.mhs_id = $profil_id"
        )->row();
    }


    // get dospem
    public function get_dospem($dt_id)
    {
        return $this->db->query(
            "SELECT
                dosen_id ,
                dtd_status ,
                dtd_id
            from
                data_ta_detail dtd
            where
                dtd.dt_id = $dt_id
            order by
                dtd_dospem_ke"
        )->result();
    }


    // add ta
    public function add($data, $jenis = '')
    {
        if (empty($jenis)) {
            $this->db->insert('data_ta', $data);
        } else {
            $this->db->insert_batch('data_ta_detail', $data);
        }

        if ($this->db->affected_rows()) {
            if (empty($jenis)) {
                $res['id'] = $this->db->insert_id();
            } else {
                $res['status'] = 'true';
            }
        } else {
            $res['status'] = 'false';
        }
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = [];
        if ($id == 'dtd_id') {
            $result = $this->db->update_batch('data_ta_detail', $data, 'dtd_id');
        } else {
            $this->db->where($this->id, $id);
            $result = $this->db->update($this->table, $data);
        }
        if ($result) {
            $res['status'] = "true";
        } else {
            $res['status'] = "false";
        }
        return $res;
    }


    // cek jurusan_code
    public function cek_data($where)
    {
        $sql = "SELECT * from $this->table where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }
}
