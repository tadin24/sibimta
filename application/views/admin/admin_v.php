<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Admin</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">NIP</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="tambahModal" tabindex="-1" aria-labelledby="tambahModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tambahModalLabel">Form Tambah Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <div class="form-group row">
            <label class="control-label col-md-3">Pegawai</label>
            <div class="col-md-9">
              <select name="user_id" id="user_id" class="form-control">
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="addData">Add</button>
      </div>
    </div>
  </div>
</div>

<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/admin/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
          "targets": [0, -1], //last column
          "orderable": false, //set not orderable
          "className": "text-center",
        }, ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  function loadTbl() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/admin/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    getPegawai()
  }


  // fungsi get pegawai
  function getPegawai() {
    $.get('<?= base_url() ?>admin/admin/get_pegawai', function(e) {
      if (e.length > 0) {
        let opt = ''
        $.each(e, function(index, i) {
          opt += `<option value="${i.user_id}">${i.pegawai_nama}</option>`
        })
        $("#user_id").html(opt)
      } else {
        $("#user_id").html('')
      }
    }, 'json')
  }


  $(document).ready(function() {
    tableAdvancedInit.init();
    loadTbl()


    // FUNGSI save
    $('#addData').click(function(e) {
      if ($("#user_id").val() == '' || $("#user_id").val() == null) {
        Swal.fire({
          icon: 'error',
          title: 'Gagal!',
          text: 'Pilih dahulu data pegawai!',
        })
        return false;
      }
      $('#addData').attr('disabled', true).text("Loading...");
      $.ajax({
        url: '<?php echo base_url('admin/admin/save') ?>',
        type: "POST",
        data: {
          user_id: $("#user_id").val()
        },
        dataType: "json",
        cache: false,
        success: function(response, status, xhr, $form) {
          var d = response;
          if (d == 'true') {
            $('#addData').attr('disabled', false).text("Add");
            Swal.fire({
              icon: 'success',
              title: 'Berhasil!',
              text: 'Data Berhasil Disimpan!',
              showConfirmButton: false,
              timer: 1500
            })
            resetForm()
          }
        }
      }, 'json');
    });


    // tombol tambah data
    $("#btnFormData").click(function() {
      getPegawai()
      $("#tambahModal").modal('show')
    })


    // event modal tambah menghilang
    $('#tambahModal').on('hidden.bs.modal', function() {
      loadTbl();
    });


    // user_id select2
    $('#user_id').select2();
  })
</script>