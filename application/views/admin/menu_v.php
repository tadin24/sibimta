<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Menu</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form id="form_vendor">
          <div class="box-body">
            <input type="hidden" name="menu_id" id="menu_id">
            <input type="hidden" name="act" id="act" value="add">
            <div class="form-group row">
              <label class="control-label col-md-3">Parent</label>
              <div class="col-md-6">
                <select class="form-control" id="parent_id" name="parent_id" placeholder="Pilih Parent">
                  <option value="0" data-level="0" data-code="">0 - ROOT</option>
                  <?php foreach ($parent as $value) : ?>
                    <option value="<?= $value->menu_id ?>" data-level="<?= $value->menu_level ?>" data-code="<?= $value->menu_code ?>"><?= $value->menu_code ?> - <?= $value->menu_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-6" style="display: none;">
                <label class="control-label" id="parent_label"></label>
              </div>
              <input type="hidden" id="parent_code" name="parent_code" value="">
              <input type="hidden" id="menu_level" name="menu_level" value="1">
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Kode</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="menu_code" name="menu_code" placeholder="Kode">
                <input type="hidden" id="menu_code_lama" name="menu_code_lama" value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="menu_name" name="menu_name" placeholder="Nama">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Jenis Menu</label>
              <div class="col-md-6">
                <select class="form-control" id="jenis_menu" name="jenis_menu">
                  <option value="1">Menu URL</option>
                  <option value="2">Menu Label</option>
                  <option value="3">Submenu</option>
                </select>
              </div>
            </div>
            <div id="menuUrlInput">
              <div class="form-group row">
                <label class="control-label col-md-3">URL</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="menu_url" name="menu_url">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Ikon</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="menu_icon" name="menu_icon">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Status</label>
              <div class="col-md-6">
                <select class="form-control" id="menu_status" name="menu_status">
                  <option value="1">Aktif</option>
                  <option value="0">Non Aktif</option>
                </select>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-danger" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Menu</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Kode</th>
                <th class="text-center">Nama</th>
                <th class="text-center">URL</th>
                <th class="text-center">Ikon</th>
                <th class="text-center">Parent</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/menu/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -2, -1],
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  function loadTbl() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi edit
  function set_val(id) {
    $.ajax({
      url: '<?= base_url() ?>admin/menu/edit/' + id,
      dataType: 'json',
      success: function(res) {
        if (!$.isEmptyObject(res.data)) {
          const data = res.data
          $("#act").val("edit")
          $("#menu_id").val(data.menu_id)
          $("#parent_id").val(data.parent_id)
          $("#parent_code").val(data.parent_code)
          $("#parent_id").closest('.col-md-6').hide()
          $("#parent_label").text(data.parent_label != '' ? data.parent_code + ' - ' + data.parent_label : '0 - ROOT')
          $("#parent_label").closest('.col-md-6').show()
          $("#menu_level").val(data.menu_level)
          $("#menu_code").val(data.menu_code)
          $("#menu_code_lama").val(data.menu_code)
          $("#menu_name").val(data.menu_name)
          $("#menu_status").val(data.menu_status)
          $("#jenis_menu").val(data.jenis_menu)
          $("#jenis_menu").trigger('change')
          if (data.jenis_menu != 2) {
            $("#menu_url").val(data.menu_url)
            $("#menu_icon").val(data.menu_icon)
          }
          $("#formContainer").slideDown(500)
          $("#tableContainer").slideUp(500)
        }
      }
    })
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/menu/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().resetForm()
    $("#menu_id").val('')
    $("#menu_level").val('1')
    $("#parent_id").closest('.col-md-6').show()
    $("#parent_id").select2('val', '0')
    $("#parent_code").val('')
    $("#parent_label").closest('.col-md-6').hide()
    $("#jenis_menu").trigger('change')
    $("#act").val('add')
  }


  // fungsi get parent
  function get_parent() {
    $.ajax({
      url: "<?= base_url() ?>admin/menu/get_parent",
      dataType: 'json',
      cache: false,
      success: function(res) {
        $("#parent_id option[value!=0]").remove()
        let opt = ''
        if (res.length > 0) {
          $.each(res, function(index, i) {
            opt += `<option value="${i.menu_id}" data-level="${i.menu_level}" data-code="${i.menu_code}">${i.menu_code} - ${i.menu_name}</option>`
          })
          $("#parent_id").append(opt)
        }
      }
    })
  }


  $(document).ready(function() {
    tableAdvancedInit.init();
    loadTbl()


    // parent_id using select2
    $("#parent_id").select2()
    // parent_id on change
    $("#parent_id").on('select2:select', function(e) {
      $("#menu_level").val(parseInt($("#parent_id").select2().find(":selected").data("level")) + 1);
      $("#parent_code").val($("#parent_id").select2().find(":selected").data("code"));
    })


    // FUNGSI save
    $('#saveData').click(function(e) {
      $('#form_vendor').submit()
    })


    // validasi data yang disimpan
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        menu_code: {
          required: true,
          remote: {
            url: "<?= base_url() ?>admin/menu/cek_data/menu_code",
            type: "post",
            data: {
              menu_code: function() {
                return $("#menu_code").val();
              },
              menu_code_lama: function() {
                return $("#menu_code_lama").val();
              },
              act: function() {
                return $("#act").val();
              },
            }
          }
        },
        menu_name: {
          required: true,
        },
      },
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");
        $.ajax({
          url: '<?php echo base_url('admin/menu/save') ?>',
          type: "POST",
          data: $('#form_vendor').serialize(),
          dataType: "json",
          cache: false,
          success: function(response, status, xhr, $form) {
            var d = response;
            if (d == 'true') {
              $('#saveData').attr('disabled', false).text("Save");
              Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              $("#cancelData").click();
            }
          }
        }, 'json');
      }
    });


    // tombol menampilkan list data
    $("#btnListData").click(function() {
      resetForm()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol batal tambah data
    $("#cancelData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol tambah data
    $("#btnFormData").click(function() {
      resetForm();
      get_parent();
      $("#tableContainer").slideUp(500)
      $("#formContainer").slideDown(500)
    })


    // jenis menu on change
    $("#jenis_menu").change(function() {
      if ($(this).val() != 2) {
        $("#menuUrlInput").show()
      } else {
        $("#menuUrlInput").hide()
      }
    })
  })
</script>