<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Grup</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form id="form_vendor">
          <div class="box-body">
            <input type="hidden" name="group_id" id="group_id">
            <input type="hidden" name="act" id="act" value="add">
            <div class="form-group row">
              <label class="control-label col-md-3">Kode</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="group_code" name="group_code" placeholder="Kode">
                <input type="hidden" id="group_code_lama" name="group_code_lama" value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="group_name" name="group_name" placeholder="Nama">
                <input type="hidden" id="group_name_lama" name="group_name_lama" value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Status</label>
              <div class="col-md-6">
                <select class="form-control" id="group_status" name="group_status">
                  <option value="1">Aktif</option>
                  <option value="0">Non Aktif</option>
                </select>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-danger" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Grup</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Kode</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Status</th>
                <th class="text-center">Akses</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="aksesModal" tabindex="-1" aria-labelledby="aksesModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="aksesModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <input type="hidden" name="akses_group_id" id="akses_group_id">
          <div class="form-group row">
            <label class="control-label col-md-3">Menu</label>
            <div class="col-md-9">
              <select name="akses_menu_id" id="akses_menu_id" class="form-control">
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <button type="button" class="btn btn-primary" id="addAkses">Tambah</button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="tree_menu"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <button type="button" class="btn btn-danger" id="hapusAkses">Hapus</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/group/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -2, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -3, -2, -1],
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  function loadTbl() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi edit
  function set_val(id) {
    $.ajax({
      url: '<?= base_url() ?>admin/group/edit/' + id,
      dataType: 'json',
      success: function(res) {
        if (!$.isEmptyObject(res.data)) {
          const data = res.data
          $("#act").val("edit")
          $("#group_id").val(data.group_id)
          $("#group_code").val(data.group_code)
          $("#group_code_lama").val(data.group_code)
          $("#group_name").val(data.group_name)
          $("#group_name_lama").val(data.group_name)
          $("#group_status").val(data.group_status)
          $("#formContainer").slideDown(500)
          $("#tableContainer").slideUp(500)
        }
      }
    })
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/group/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().resetForm()
    $("#group_id").val('')
    $("#act").val('add')
  }


  // fungsi setting hak akses
  function set_akses(id, title = '') {
    $("#akses_group_id").val(id)
    $("#aksesModalLabel").text(title)
    get_menu()
    $('#tree_menu').jstree("refresh");
    $("#aksesModal").modal('show')
  }


  // get_menu
  function get_menu() {
    $.ajax({
      url: "<?= base_url() ?>admin/group/get_menu",
      data: {
        group_id: $("#akses_group_id").val()
      },
      dataType: 'json',
      success: function(res) {
        let opt = ''
        if (res.length > 0) {
          $.each(res, function(index, i) {
            opt += `<option value="${i.menu_id}">${i.menu_code} - ${i.menu_name}</option>`
          })
          $("#akses_menu_id").html(opt)
        } else {
          $("#akses_menu_id").html('')
        }
      }
    })
  }


  $(document).ready(function() {
    tableAdvancedInit.init();
    loadTbl()
    $('#tree_menu').jstree({
      'plugins': ["checkbox", "types"],
      'core': {
        "themes": {
          "responsive": false,
          "icons": false,
        },
        "check_callback": true,
        'data': {
          'url': function(node) {
            return '<?= base_url() ?>admin/group/get_tree_menu'
          },
          'data': function(node) {
            return {
              'group_id': $("#akses_group_id").val(),
              'id': node.id,
            }
          },
          'dataType': 'json',
        },
        checkbox: {
          tie_selection: false
        },
      },
    })


    // FUNGSI save
    $('#saveData').click(function(e) {
      $('#form_vendor').submit()
    })


    // validasi data yang disimpan
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        group_code: {
          required: true,
          remote: {
            url: "<?= base_url() ?>admin/group/cek_data/group_code",
            type: "post",
            data: {
              group_code: function() {
                return $("#group_code").val();
              },
              group_code_lama: function() {
                return $("#group_code_lama").val();
              },
              act: function() {
                return $("#act").val();
              },
            }
          }
        },
        group_name: {
          required: true,
          remote: {
            url: "<?= base_url() ?>admin/group/cek_data/group_name",
            type: "post",
            data: {
              group_name: function() {
                return $("#group_name").val();
              },
              group_name_lama: function() {
                return $("#group_name_lama").val();
              },
              act: function() {
                return $("#act").val();
              },
            }
          }
        },
      },
      messages: {
        group_code: {
          remote: 'Kode sudah digunakan'
        }
      },
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");
        $.ajax({
          url: '<?php echo base_url('admin/group/save') ?>',
          type: "POST",
          data: $('#form_vendor').serialize(),
          dataType: "json",
          cache: false,
          success: function(response, status, xhr, $form) {
            var d = response;
            if (d == 'true') {
              $('#saveData').attr('disabled', false).text("Save");
              Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              $("#cancelData").click();
            }
          }
        }, 'json');
      }
    });


    // tombol menampilkan list data
    $("#btnListData").click(function() {
      resetForm()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol batal tambah data
    $("#cancelData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol tambah data
    $("#btnFormData").click(function() {
      $("#tableContainer").slideUp(500)
      $("#formContainer").slideDown(500)
    })


    // add menu
    $("#addAkses").click(function() {
      $.ajax({
        url: "<?= base_url() ?>admin/group/tambah_akses",
        data: {
          group_id: $("#akses_group_id").val(),
          menu_id: $("#akses_menu_id").val(),
        },
        dataType: 'json',
        type: 'post',
        success: function(res) {
          $("#tree_menu").jstree('refresh')
          get_menu()
          Swal.fire({
            icon: 'success',
            title: 'Berhasil!',
            text: '1 menu telah ditambahkan!',
            showConfirmButton: false,
            timer: 1500
          })
        }
      })
    })


    // hapus menu
    $("#hapusAkses").click(function() {
      if ($("#tree_menu li a.jstree-clicked").length > 0) {
        Swal.fire({
          title: 'Apakah Anda yakin?',
          text: "Data yang terhapus tidak bisa dikembalikan!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Iya!'
        }).then((result) => {
          if (result.value) {
            let menu_id = []
            $.each($("#tree_menu li a.jstree-clicked"), function(index, i) {
              menu_id.push($(i).attr('menu_id'))
            })
            $.ajax({
              url: "<?= base_url() ?>admin/group/hapus_akses",
              data: {
                group_id: $("#akses_group_id").val(),
                menu_id: menu_id,
              },
              dataType: 'json',
              type: 'post',
              success: function(res) {
                $("#tree_menu").jstree('refresh')
                get_menu()
                Swal.fire({
                  icon: 'success',
                  title: 'Berhasil!',
                  text: 'Menu telah dihapus!',
                  showConfirmButton: false,
                  timer: 1500
                })
              }
            })
          }
        })

      } else {
        Swal.fire({
          icon: 'error',
          title: 'Gagal!',
          text: 'Pilih minimal 1 menu!',
        })
      }
    })
  })
</script>