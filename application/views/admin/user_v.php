<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master User</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form id="form_vendor">
          <div class="box-body">
            <input type="hidden" name="user_id" id="user_id">
            <input type="hidden" name="act" id="act" value="add">
            <div class="form-group row">
              <label class="control-label col-md-3">Jenis User</label>
              <div class="col-md-6">
                <select name="jenis_profil" id="jenis_profil" class="form-control">
                  <option value="2">Pegawai</option>
                  <option value="1">Mahasiswa</option>
                </select>
              </div>
            </div>
            <div class="form-group row" style="display: none;">
              <label class="control-label col-md-3">Jurusan</label>
              <div class="col-md-6">
                <select name="jurusan_id" id="jurusan_id" class="form-control">
                  <?php foreach ($jurusan as $v) : ?>
                    <option value="<?= $v->jurusan_id ?>"><?= $v->jurusan_name ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Nama Lengkap User</label>
              <div class="col-md-6">
                <select class="form-control" id="profil_id" name="profil_id"></select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Username</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                <input type="hidden" id="username_lama" name="username_lama" value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" id="password" name="password">
              </div>
              <div class="col-md-2" id="inputGantiPassword" style="display: none;">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="gantiPassword" name="gantiPassword">
                  <label class="custom-control-label" for="gantiPassword">Ganti Password</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Ulangi Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" id="password2" name="password2">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Status</label>
              <div class="col-md-6">
                <select class="form-control" id="user_status" name="user_status">
                  <option value="1">Aktif</option>
                  <option value="0">Non Aktif</option>
                </select>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-danger" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master User</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="control-label col-md-3">Jenis Profil</label>
                <div class="col-md-9">
                  <select name="f_jenis_profil" id="f_jenis_profil" class="form-control">
                    <option value="2">Pegawai</option>
                    <option value="1">Mahasiswa</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Username</th>
                <th class="text-center">Nama Lengkap</th>
                <th class="text-center">Status</th>
                <th class="text-center">Akses</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="aksesModal" tabindex="-1" aria-labelledby="aksesModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="aksesModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <input type="hidden" name="akses_user_id" id="akses_user_id">
          <div class="form-group row">
            <label class="control-label col-md-3">Group</label>
          </div>
          <div class="form-group row">
            <div class="col-md-12" id="listGroup"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addAkses">Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/user/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          "data": function(d) {
            d.f_jenis_profil = $('#f_jenis_profil').val();
          },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -3, -2, -1],
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  function loadTbl() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi edit
  function set_val(id, jenis_profil) {
    $.ajax({
      url: '<?= base_url() ?>admin/user/edit',
      data: {
        id: id,
        jenis_profil: jenis_profil,
      },
      dataType: 'json',
      success: function(res) {
        if (!$.isEmptyObject(res.data)) {
          const data = res.data
          $("#act").val("edit")
          $("#user_id").val(data.user_id)
          $("#jenis_profil").val(data.jenis_profil)
          $("#jenis_profil").trigger('change')
          if (data.jenis_profil == 1) {
            $("#jurusan_id").val(data.jurusan_id)
          }
          $("#profil_id").html(`<option value="${data.profil_id}">${data.user_fullname}</option>`)
          $("#profil_id").select2('data', {
            id: data.profil_id,
            text: data.user_fullname
          })
          $("#username").val(data.username)
          $("#username_lama").val(data.username)
          $("#user_status").val(data.user_status)
          $("#inputGantiPassword").show()
          $("#password").attr('disabled', 'disabled')
          $("#password2").attr('disabled', 'disabled')
          $("#formContainer").slideDown(500)
          $("#tableContainer").slideUp(500)
        }
      }
    })
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/user/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().resetForm()
    $("#user_id").val('')
    $("#profil_id").html('')
    $("#act").val('add')
    $("#password").removeAttr('disabled', 'disabled')
    $("#password2").removeAttr('disabled', 'disabled')
    $("#inputGantiPassword").hide()
  }


  // fungsi setting hak akses
  function set_akses(id, title = '') {
    $("#akses_user_id").val(id)
    $("#aksesModalLabel").text(title)
    get_user_akses()
    $("#aksesModal").modal('show')
  }


  // get user akses
  function get_user_akses() {
    const user_id = $("#akses_user_id").val()
    $("#listGroup").html('Loading...')
    $.ajax({
      url: "<?= base_url() ?>admin/user/list_group",
      data: {
        id: user_id,
      },
      dataType: 'json',
      success: function(res) {
        if (res.length > 0) {
          let li = '',
            checked = ''
          $.each(res, function(index, i) {
            checked = i.checked == 1 ? 'checked' : ''
            li +=
              `<div class="form-check">
                <label class="form-check-label" for="checkbox${index}">
                  <input class="form-check-input" type="checkbox" name="group_id[]" id="checkbox${index}" value="${i.group_id}" ${checked}>
                  ${i.group_name}
                </label>
              </div>`
          })
          $("#listGroup").html(li)
        } else {
          $("#listGroup").html('')
        }

      }
    })
  }


  $(document).ready(function() {
    tableAdvancedInit.init();
    loadTbl()


    // FUNGSI save
    $('#saveData').click(function(e) {
      $('#form_vendor').submit()
    })


    // validasi data yang disimpan
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        username: {
          required: true,
          remote: {
            url: "<?= base_url() ?>admin/user/cek_data/username",
            type: "post",
            data: {
              username: function() {
                return $("#username").val();
              },
              username_lama: function() {
                return $("#username_lama").val();
              },
              act: function() {
                return $("#act").val();
              },
            }
          }
        },
        password: {
          required: true,
        },
        password2: {
          required: true,
          equalTo: "#password"
        }
      },
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");
        $.ajax({
          url: '<?php echo base_url('admin/user/save') ?>',
          type: "POST",
          data: $('#form_vendor').serialize(),
          dataType: "json",
          cache: false,
          success: function(response, status, xhr, $form) {
            var d = response;
            if (d == 'true') {
              $('#saveData').attr('disabled', false).text("Save");
              Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              $("#cancelData").click();
            }
          }
        }, 'json');
      }
    });


    // tombol menampilkan list data
    $("#btnListData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol batal tambah data
    $("#cancelData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol tambah data
    $("#btnFormData").click(function() {
      $("#tableContainer").slideUp(500)
      $("#formContainer").slideDown(500)
    })


    // filter jenis profil
    $("#f_jenis_profil").change(function() {
      loadTbl()
    })


    // jenis profil
    $("#jenis_profil").change(function() {
      if ($(this).val() == 2) {
        $("#jurusan_id").closest('.form-group.row').hide()
      } else {
        $("#jurusan_id").closest('.form-group.row').show()
      }
    })


    // input checkbox on change
    $("#gantiPassword").change(function() {
      if ($(this).prop("checked")) {
        $("#password").removeAttr("disabled", "disabled")
        $("#password2").removeAttr("disabled", "disabled")
      } else {
        $("#password").attr("disabled", "disabled")
        $("#password2").attr("disabled", "disabled")
      }
    })


    // add menu
    $("#addAkses").click(function() {
      let group_id = []
      const list_group = $("#listGroup .form-check-input:checked");
      if (list_group.length > 0) {
        $.each(list_group, function(index, i) {
          group_id.push($(i).val())
        })
        $.ajax({
          url: "<?= base_url() ?>admin/user/simpan_akses",
          data: {
            user_id: $("#akses_user_id").val(),
            group_id: group_id,
          },
          dataType: 'json',
          type: 'post',
          success: function(res) {
            Swal.fire({
              icon: 'success',
              title: 'Berhasil!',
              text: 'Group telah disimpan!',
              showConfirmButton: false,
              timer: 1500
            })
          }
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Gagal!',
          text: 'Pilih minimal 1 Group!',
        })
      }
    })


    // mencari data profil
    $('#profil_id').select2({
      placeholder: 'Ketikkan nama lengkap user',
      minimumInputLength: 1,
      ajax: {
        url: '<?= base_url() ?>admin/user/get_profil',
        delay: 250,
        data: function(params) {
          var query = {
            search: params.term,
            jenis_profil: $("#jenis_profil").val(),
            jurusan_id: $("#jurusan_id").val()
          }

          // Query parameters will be ?search=[term]&type=public
          return query;
        },
        dataType: 'json',
        processResults: function(data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          return {
            results: data
          };
        },
      }
    });
  })
</script>