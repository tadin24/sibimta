<div class="row">
  <div class="col-lg-6">
    <div class="form-group row">
      <label class="control-label col-md-3">Jenis Profil</label>
      <div class="col-md-9">
        <select id="f_jenis_profil" class="form-control">
          <option value="2">Pegawai</option>
          <option value="1">Mahasiswa</option>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label class="control-label col-md-3">Jenis Grup</label>
      <div class="col-md-9">
        <select id="f_group_id" class="form-control">
          <?php foreach ($group as $value) : ?>
            <option value="<?= $value->group_id ?>"><?= $value->group_name ?></option>
          <?php endforeach ?>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="row" id="tableContainer">
  <div class="col-lg-5">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Data User</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover" id="table-kiri" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center"><input id="cekAllKiri" type="checkbox"></th>
                <th class="text-center">Username</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-2 text-center pt-5 mt-5">
    <div class="row">
      <div class="col-md-12">
        <button class="btn btn-primary" onclick="pindahKanan()" id="pindahKanan">
          >
        </button>
      </div>
    </div>
    &nbsp;
    <div class="row">
      <div class="col-md-12">
        <button class="btn btn-primary" onclick="pindahKiri()" id="pindahKiri">
          < </button>
      </div>
    </div>
  </div>
  <div class="col-lg-5">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Data User di Group</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover" id="table-kanan" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center"><input type="checkbox" id="cekAllKanan"></th>
                <th class="text-center">Username</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTableKanan = function() {
      var target = '#table-kanan';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100],
          [10, 25, 50, 100]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/user_in_group/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          "data": function(d) {
            d.dari = 'kanan';
            d.f_group_id = $('#f_group_id').val();
            d.f_jenis_profil = $('#f_jenis_profil').val();
          },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, 1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -2, -1],
            "className": "text-center"
          },
          {
            "targets": [0],
            "width": "10%",
          },
          {
            "targets": [1],
            "width": "30%"
          },
          {
            "targets": [2],
            "width": "60%"
          },
        ],
        "order": [
          [2, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    var initTableKiri = function() {
      var target = '#table-kiri';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/user_in_group/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          "data": function(d) {
            d.dari = 'kiri';
            d.f_jenis_profil = $('#f_jenis_profil').val();
            d.f_group_id = $('#f_group_id').val();
          },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, 1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -2, -1],
            "className": "text-center"
          },
          {
            "targets": [0],
            "width": "10%",
          },
          {
            "targets": [1],
            "width": "30%"
          },
          {
            "targets": [2],
            "width": "60%"
          },
        ],
        "order": [
          [2, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTableKiri();
        initTableKanan();
      }
    };
  }();


  // fungsi muat ulang table-kiri
  function loadTbl() {
    $("#table-kiri").dataTable().fnDraw();
    $("#table-kanan").dataTable().fnDraw();
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/jurusan/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    // $("#form_vendor")[0].reset()
    // $("#form_vendor").validate().resetForm()
    // $("#jurusan_id,#jurusan_code_lama,#jurusan_name_lama").val('')
    // $("#act").val('add')
  }


  function pindahKanan() {
    const data = $("#table-kiri tbody tr .user-id:checked")
    if (data.length > 0) {
      $("#pindahKanan").attr('disabled', 'disabled')
      const user_id = [];
      $.each(data, function(index, i) {
        user_id.push($(i).val())
      })
      $.post('<?= base_url() ?>admin/user_in_group/save', {
        user_id: user_id,
        group_id: $("#f_group_id").val()
      }, function(res) {
        $("#pindahKanan").removeAttr('disabled', 'disabled')
        if (res.status == 'true') {
          Swal.fire(
            'Berhasil!',
            'User telah ditambahkan',
            'success'
          )
          loadTbl()
        } else {
          Swal.fire(
            'Gagal!',
            res.msg,
            'error'
          )
        }
      }, 'json')
    } else {
      Swal.fire(
        'Gagal!',
        'Pilih minimal 1 user di sebelah kiri',
        'error'
      )
    }
  }


  function pindahKiri() {
    const data = $("#table-kanan tbody tr .user-id:checked")
    if (data.length > 0) {
      $("#pindahKiri").attr('disabled', 'disabled')
      const user_id = [];
      $.each(data, function(index, i) {
        user_id.push($(i).val())
      })
      $.post('<?= base_url() ?>admin/user_in_group/hapus', {
        user_id: user_id,
        group_id: $("#f_group_id").val()
      }, function(res) {
        $("#pindahKiri").removeAttr('disabled', 'disabled')
        if (res.status == 'true') {
          Swal.fire(
            'Berhasil!',
            'User telah dihapus',
            'success'
          )
          loadTbl()
        } else {
          Swal.fire(
            'Gagal!',
            res.msg,
            'error'
          )
        }
      }, 'json')
    } else {
      Swal.fire(
        'Gagal!',
        'Pilih minimal 1 user di sebelah kanan',
        'error'
      )
    }
  }


  $(document).ready(function() {
    tableAdvancedInit.init();

    $("#f_jenis_profil,#f_group_id").change(function() {
      loadTbl()
    })

    $("#cekAllKiri").click(function() {
      if ($(this).prop('checked')) {
        $('#table-kiri tbody').find('.user-id').prop('checked', true)
      } else {
        $('#table-kiri tbody').find('.user-id').prop('checked', false)
      }
    })

    $("#cekAllKanan").click(function() {
      if ($(this).prop('checked')) {
        $('#table-kanan tbody').find('.user-id').prop('checked', true)
      } else {
        $('#table-kanan tbody').find('.user-id').prop('checked', false)
      }
    })
  })
</script>