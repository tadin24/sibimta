<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Mahasiswa</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form id="form_vendor">
          <div class="box-body">
            <input type="hidden" name="mhs_id" id="mhs_id">
            <input type="hidden" name="act" id="act" value="add">
            <div class="form-group row">
              <label class="control-label col-md-3">Jurusan</label>
              <div class="col-md-6">
                <select class="form-control" id="jurusan_id" name="jurusan_id">
                  <?php foreach ($jurusan as $key => $value) : ?>
                    <option value="<?= $value->jurusan_id ?>"><?= $value->jurusan_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">NIM</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="mhs_nim" name="mhs_nim" placeholder="NIM">
                <input type="hidden" id="mhs_nim_lama" name="mhs_nim_lama" value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="mhs_name" name="mhs_name" placeholder="Nama">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Semester</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="mhs_smt" name="mhs_smt" placeholder="Semester">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">SKS</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="mhs_sks" name="mhs_sks" placeholder="Total SKS">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">No. HP</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Nomor HP">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Foto</label>
              <div class="col-md-6">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" accept="image/png,image/jpg,image/jpeg" id="photo" name="photo">
                  <label class="custom-file-label" for="photo">Choose file</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-md-3 col-md-3">
                <img width="200" id="imagePreview" src="<?= base_url() ?>assets/global/images/profil.jpg" class="rounded">
              </div>
              <div class="col-md-3">
                <input type="hidden" name="is_delete" id="is_delete" value="no">
                <button type="button" class="btn btn-danger" id="hapusImage" title="hapus gambar"><i class="fa fa-trash"></i></button>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Status</label>
              <div class="col-md-6">
                <select class="form-control" id="mhs_status" name="mhs_status">
                  <option value="1">Aktif</option>
                  <option value="0">Non Aktif</option>
                </select>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-danger" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Mahasiswa</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">NIM</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/mahasiswa/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -2, -1],
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  function loadTbl() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi edit
  function set_val(id) {
    $.ajax({
      url: '<?= base_url() ?>admin/mahasiswa/edit/' + id,
      dataType: 'json',
      success: function(res) {
        if (!$.isEmptyObject(res.data)) {
          const data = res.data
          $("#act").val("edit")
          $("#mhs_id").val(data.mhs_id)
          $("#jurusan_id").val(data.jurusan_id)
          $("#mhs_nim").val(data.mhs_nim)
          $("#mhs_nim_lama").val(data.mhs_nim)
          $("#mhs_name").val(data.mhs_name)
          $("#mhs_smt").val(data.mhs_smt)
          $("#mhs_sks").val(data.mhs_sks)
          $("#email").val(data.email)
          $("#phone").val(data.phone)
          if (data.photo != '' && data.photo != 'null' && data.photo != null) {
            $("#photo").next().text(data.photo)
            $("#imagePreview").attr('src', '<?= base_url() ?>assets/global/images/mahasiswa/' + data.photo)
          } else {
            $("#photo").next().text('Choose file')
            $("#imagePreview").attr('src', '<?= base_url() ?>assets/global/images/profil.jpg')
          }
          $("#mhs_status").val(data.mhs_status)
          $("#formContainer").slideDown(500)
          $("#tableContainer").slideUp(500)
        }
      }
    })
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/mahasiswa/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().resetForm()
    $("#mhs_id,#mhs_nim_lama").val('')
    $("#is_delete").val('no')
    $("#imagePreview").attr('src', "<?= base_url() ?>assets/global/images/profil.jpg")
    $("#photo").next().text("Choose file")
    $("#act").val('add')
  }


  // baca url image
  function readURL() {
    var input = $("#photo")[0];
    var url = $("#photo").val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#imagePreview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    } else {
      $('#imagePreview').attr('src', '<?= base_url() ?>assets/global/images/profil.jpg');
    }
  }


  $(document).ready(function() {
    tableAdvancedInit.init();
    loadTbl()


    // FUNGSI save
    $('#saveData').click(function(e) {
      $('#form_vendor').submit()
    })


    // validasi data yang disimpan
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        mhs_nim: {
          required: true,
          remote: {
            url: "<?= base_url() ?>admin/mahasiswa/cek_data/mhs_nim",
            type: "post",
            data: {
              mhs_nim: function() {
                return $("#mhs_nim").val();
              },
              mhs_nim_lama: function() {
                return $("#mhs_nim_lama").val();
              },
              act: function() {
                return $("#act").val();
              },
            }
          }
        },
        mhs_name: {
          required: true,
        },
      },
      submitHandler: function(form) {
        let data = new FormData(form)
        if ($("#photo")[0].files.length <= 0) {
          data.delete('photo')
        }

        $('#saveData').attr('disabled', true).text("Loading...");
        $.ajax({
          url: '<?php echo base_url('admin/mahasiswa/save') ?>',
          type: "POST",
          data: data,
          dataType: "json",
          cache: false,
          contentType: false,
          processData: false,
          error: function(res) {
            $('#saveData').attr('disabled', false).text("Save");
          },
          success: function(response, status, xhr, $form) {
            var d = response;
            $('#saveData').attr('disabled', false).text("Save");
            if (d.status == 'true') {
              Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              $("#cancelData").click();
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Gagal!',
                text: d.msg,
              })
            }
          }
        }, 'json');
      }
    });


    // tombol menampilkan list data
    $("#btnListData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol batal tambah data
    $("#cancelData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol tambah data
    $("#btnFormData").click(function() {
      $("#tableContainer").slideUp(500)
      $("#formContainer").slideDown(500)
    })


    // 
    $("#photo").change(function() {
      readURL()
      $(this).next().text($(this).val())
      $("#is_delete").val("no")
    })


    // 
    $("#hapusImage").click(function() {
      $("#photo").val("")
      $("#is_delete").val("yes")
      $("#photo").next().text("Choose file")
      $("#imagePreview").attr("src", "<?= base_url() ?>assets/global/images/profil.jpg")
    })
  })
</script>