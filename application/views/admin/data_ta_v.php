<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Data Tugas Akhir</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form id="form_vendor">
          <div class="box-body">
            <input type="hidden" name="act" id="act" value="add">
            <input type="hidden" name="dt_id" id="dt_id" value="">
            <div class="form-group row">
              <label class="control-label col-md-3">Jurusan</label>
              <div class="col-md-3">
                <select name="jurusan_id" id="jurusan_id" class="form-control">
                  <?php foreach ($jurusan as $v) : ?>
                    <option value="<?= $v->jurusan_id ?>"><?= $v->jurusan_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <label class="col-md-3" id="jurusan_label"></label>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-6">
                <select name="mhs_id" id="mhs_id" class="form-control"></select>
              </div>
              <label class="col-md-6" id="nama_label"></label>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Judul</label>
              <div class="col-md-9">
                <input type="text" name="dt_judul" id="dt_judul" class="form-control" value="">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Dosen Pembimbing</label>
            </div>

            <div class="row">
              <div class="col-sm-11">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <td class="text-center">No</td>
                      <td class="text-center">Nama Dosen Pembimbing</td>
                      <td class="text-center aksi" style="display: none;">Aksi</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center">1</td>
                      <td>
                        <div class="form-group">
                          <input type="hidden" name="detail[0][dtd_id]" id="dtd_id1">
                          <select name="detail[0][dosen_id]" id="dosen_id1" class="form-control pembimbing">
                            <option value="">Pilih Dosen Pembimbing 1</option>
                            <?php foreach ($dosen as $pembimbing1) : ?>
                              <option value="<?= $pembimbing1->pegawai_id ?>"><?= $pembimbing1->pegawai_nama ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </td>
                      <td class="text-center aksi" style="display: none;">
                        <div class="form-group">
                          <select name="detail[0][dtd_status]" id="dtd_status1" class="form-control">
                            <option value="0">Proses</option>
                            <option value="2">Disetujui</option>
                            <option value="1">Ditolak</option>
                          </select>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-center">2</td>
                      <td>
                        <div class="form-group">
                          <input type="hidden" name="detail[1][dtd_id]" id="dtd_id2">
                          <select name="detail[1][dosen_id]" id="dosen_id2" class="form-control pembimbing">
                            <option value="">Pilih Dosen Pembimbing 2</option>
                            <?php foreach ($dosen as $pembimbing2) : ?>
                              <option value="<?= $pembimbing2->pegawai_id ?>"><?= $pembimbing2->pegawai_nama ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </td>
                      <td class="text-center aksi" style="display: none;">
                        <div class="form-group">
                          <select name="detail[1][dtd_status]" id="dtd_status2" class="form-control">
                            <option value="0">Proses</option>
                            <option value="2">Disetujui</option>
                            <option value="1">Ditolak</option>
                          </select>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Sinopsis</label>
              <div class="col-md-6">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" accept="application/msword,application/pdf" id="dt_sinopsis" name="dt_sinopsis">
                  <label class="custom-file-label" for="dt_sinopsis">Pilih File</label>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-danger" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Data Tugas Akhir</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="control-label col-md-3">Jurusan</label>
                <div class="col-md-9">
                  <select name="f_jurusan_id" id="f_jurusan_id" class="form-control">
                    <?php foreach ($jurusan as $v) : ?>
                      <option value="<?= $v->jurusan_id ?>"><?= $v->jurusan_name ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="control-label col-md-3">Status</label>
                <div class="col-md-9">
                  <select name="f_status" id="f_status" class="form-control">
                    <option value="0">Proses</option>
                    <option value="2">Disetujui</option>
                    <option value="1">Ditolak</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Dosen 1</th>
                <th class="text-center">Dosen 2</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/data_ta/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          "data": function(d) {
            d.f_jurusan_id = $('#f_jurusan_id').val();
            d.f_status = $('#f_status').val();
          },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, 2, 3, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -2, -1],
            "className": "text-center"
          },
          // {
          //   "targets": [0],
          //   "width": "5%",
          // },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  function loadTbl() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi edit
  function set_val(id) {
    $.ajax({
      url: '<?= base_url() ?>admin/data_ta/edit/' + id,
      dataType: 'json',
      success: function(res) {
        if (!$.isEmptyObject(res.data)) {
          const data = res.data
          $("#act").val("edit")
          $("#dt_id").val(data.dt_id)
          $("#jurusan_id").closest('.col-md-3').hide()
          $("#jurusan_label").show()
          $("#jurusan_label").text(data.jurusan_name)
          $("#mhs_id").closest('.col-md-6').hide()
          $("#nama_label").show()
          $("#nama_label").text(data.mhs_name)
          $("#dt_judul").val(data.dt_judul)
          $("#dt_sinopsis").next().text(data.dt_sinopsis_name == null ? 'Pilih File' : data.dt_sinopsis_name)
          $.each(data.detail, function(index, i) {
            $('[name=\'detail[' + index + '][dosen_id]\']').val(i.dosen_id)
            $('[name=\'detail[' + index + '][dtd_id]\']').val(i.dtd_id)
            $('[name=\'detail[' + index + '][dtd_status]\']').val(i.dtd_status)
          })
          $('.aksi').show()
          $("#formContainer").slideDown(500)
          $("#tableContainer").slideUp(500)
        }
      }
    })
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/data_ta/hapus/' + id,
          dataType: "json",
          success: function(res) {
            if (res.status == 'true') {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().resetForm()
    $("#act").val('add')
    $("#dt_id,#dtd_id1,#dtd_id2").val('')
    $('#dt_sinopsis').next().text('Pilih File')
    $("#jurusan_id").closest('.col-md-3').show()
    $("#jurusan_label").hide()
    $("#mhs_id").closest('.col-md-6').show()
    $("#nama_label").hide()
    $('.aksi').hide()
  }


  // mengambil data mahasiswa
  const getMahasiswa = (jurusan_id) => {
    $.ajax({
      url: "<?= base_url() ?>admin/data_ta/get_mahasiswa/" + jurusan_id,
      dataType: 'json',
      success: res => {
        let opt = '';
        if (res.length > 0) {
          $.each(res, (index, item) => {
            opt += `<option value="${item.mhs_id}" >${item.mhs_name}</option>`
          })
          $("#mhs_id").html(opt)
        } else {
          $("#mhs_id").html(opt)
        }
      }
    })
  }


  $.validator.addMethod('filesize', function(value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
  }, 'Ukuran File harus kurang dari 4MB');


  $(document).ready(function() {
    tableAdvancedInit.init();


    // FUNGSI save
    $('#saveData').click(function(e) {
      $('#form_vendor').submit()
    })


    // validasi data yang disimpan
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        // jurusan_code: {
        //   required: true,
        //   remote: {
        //     url: "<?= base_url() ?>admin/data_ta/cek_data/jurusan_code",
        //     type: "post",
        //     data: {
        //       jurusan_code: function() {
        //         return $("#jurusan_code").val();
        //       },
        //       jurusan_code_lama: function() {
        //         return $("#jurusan_code_lama").val();
        //       },
        //       act: function() {
        //         return $("#act").val();
        //       },
        //     }
        //   }
        // },
        dt_judul: {
          required: true
        },
        'detail[0][dosen_id]': {
          required: true
        },
        'detail[1][dosen_id]': {
          required: true
        },
        dt_sinopsis: {
          filesize: 4096000
        }
      },
      submitHandler: function(form) {
        if ($("#dosen_id1").val() == $("#dosen_id2").val()) {
          Swal.fire({
            icon: 'error',
            title: 'Error!',
            text: 'Dosen Pembimbing 1 tidak boleh sama dengan Dosen Pembimbing 2!'
          })
          return false;
        }
        $('#saveData').attr('disabled', true).text("Loading...");
        let data = new FormData(form)
        if ($("#dt_sinopsis")[0].files.length <= 0) {
          data.delete('dt_sinopsis')
        }
        $.ajax({
          url: '<?php echo base_url('admin/data_ta/save') ?>',
          type: "POST",
          data: data,
          dataType: "json",
          cache: false,
          contentType: false,
          processData: false,
          error: function() {
            $('#saveData').attr('disabled', false).text("Save");
          },
          complete: function() {
            $('#saveData').attr('disabled', false).text("Save");
          },
          success: function(response, status, xhr, $form) {
            var d = response;
            if (d.status == 'true') {
              $('#saveData').attr('disabled', false).text("Save");
              Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              $("#cancelData").click();
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Gagal!',
                html: d.msg,
              })
            }
          }
        }, 'json');
      }
    });


    // tombol menampilkan list data
    $("#btnListData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol batal tambah data
    $("#cancelData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol tambah data
    $("#btnFormData").click(function() {
      resetForm()
      $("#jurusan_id").trigger('change')
      $("#tableContainer").slideUp(500)
      $("#formContainer").slideDown(500)
    })


    // filter change
    $("#f_jurusan_id,#f_status").change(function() {
      loadTbl()
    })


    $("#jurusan_id").change(function() {
      getMahasiswa($(this).val())
    })


    $("#jurusan_id").trigger('change')


    $("#mhs_id").select2()


    // menu daftar ta
    $("#dt_sinopsis").change(function() {
      // readURL(this)

      if (this.files && this.files[0]) {
        $(this).next('label').text($(this)[0].files[0].name);
      }
    })
  })
</script>