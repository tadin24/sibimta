<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Pegawai</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form id="form_vendor">
          <div class="box-body">
            <input type="hidden" name="pegawai_id" id="pegawai_id">
            <input type="hidden" name="act" id="act" value="add">
            <div class="form-group row">
              <label class="control-label col-md-3">Kategori Pegawai</label>
              <div class="col-md-6">
                <select class="form-control" id="kp_id" name="kp_id">
                  <?php foreach ($kategori as $key => $value) : ?>
                    <option value="<?= $value->kp_id ?>"><?= $value->kp_code ?> - <?= $value->kp_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="pegawai_nama" name="pegawai_nama" placeholder="Nama">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">NIP</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="pegawai_nip" name="pegawai_nip" placeholder="NIP">
                <input type="hidden" id="pegawai_nip_lama" name="pegawai_nip_lama" value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">NIDN</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="pegawai_nidn" name="pegawai_nidn" placeholder="NIDN">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Gelar Depan</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="pegawai_gd" name="pegawai_gd" placeholder="Gelar Depan">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Gelar Belakang</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="pegawai_gb" name="pegawai_gb" placeholder="Gelar Belakang">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">No. HP</label>
              <div class="col-md-6">
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Nomor HP">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Foto</label>
              <div class="col-md-6">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" accept="image/png,image/jpg,image/jpeg" id="photo" name="photo">
                  <label class="custom-file-label" for="photo">Choose file</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-md-3 col-md-3">
                <img width="200" id="imagePreview" src="<?= base_url() ?>assets/global/images/profil.jpg" class="rounded">
              </div>
              <div class="col-md-3">
                <input type="hidden" name="is_delete" id="is_delete" value="no">
                <button type="button" class="btn btn-danger" id="hapusImage" title="hapus gambar"><i class="fa fa-trash"></i></button>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Status</label>
              <div class="col-md-6">
                <select class="form-control" id="pegawai_status" name="pegawai_status">
                  <option value="1">Aktif</option>
                  <option value="0">Non Aktif</option>
                </select>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-danger" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Pegawai</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">NIP</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // inisialisasi tabel
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('admin/pegawai/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -2, -1],
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  function loadTbl() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi edit
  function set_val(id) {
    $.ajax({
      url: '<?= base_url() ?>admin/pegawai/edit/' + id,
      dataType: 'json',
      success: function(res) {
        if (!$.isEmptyObject(res.data)) {
          const data = res.data
          $("#act").val("edit")
          $("#pegawai_id").val(data.pegawai_id)
          $("#kp_id").val(data.kp_id)
          $("#pegawai_nama").val(data.pegawai_nama)
          $("#pegawai_nip").val(data.pegawai_nip)
          $("#pegawai_nip_lama").val(data.pegawai_nip)
          $("#pegawai_nidn").val(data.pegawai_nidn)
          $("#pegawai_gd").val(data.pegawai_gd)
          $("#pegawai_gb").val(data.pegawai_gb)
          $("#email").val(data.email)
          $("#phone").val(data.phone)
          if (data.photo != '' && data.photo != 'null' && data.photo != null) {
            $("#photo").next().text(data.photo)
            $("#imagePreview").attr('src', '<?= base_url() ?>assets/global/images/pegawai/' + data.photo)
          } else {
            $("#photo").next().text('Choose file')
            $("#imagePreview").attr('src', '<?= base_url() ?>assets/global/images/profil.jpg')
          }
          $("#pegawai_status").val(data.pegawai_status)
          $("#formContainer").slideDown(500)
          $("#tableContainer").slideUp(500)
        }
      }
    })
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/pegawai/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().resetForm()
    $("#pegawai_id,#pegawai_nip_lama").val('')
    $("#kp_id").select2('val', $("#kp_id option:first").val())
    $("#is_delete").val('no')
    $("#imagePreview").attr('src', "<?= base_url() ?>assets/global/images/profil.jpg")
    $("#photo").next().text("Choose file")
    $("#act").val('add')
  }


  // baca url image
  function readURL() {
    var input = $("#photo")[0];
    var url = $("#photo").val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#imagePreview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    } else {
      $('#imagePreview').attr('src', '<?= base_url() ?>assets/global/images/profil.jpg');
    }
  }


  $(document).ready(function() {
    tableAdvancedInit.init();
    loadTbl()


    // FUNGSI save
    $('#saveData').click(function(e) {
      $('#form_vendor').submit()
    })


    // validasi data yang disimpan
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        pegawai_nip: {
          remote: {
            url: "<?= base_url() ?>admin/pegawai/cek_data/pegawai_nip",
            type: "post",
            data: {
              pegawai_nip: function() {
                return $("#pegawai_nip").val();
              },
              pegawai_nip_lama: function() {
                return $("#pegawai_nip_lama").val();
              },
              act: function() {
                return $("#act").val();
              },
            }
          }
        },
        pegawai_nama: {
          required: true,
        },
      },
      submitHandler: function(form) {
        let data = new FormData(form)
        if ($("#photo")[0].files.length <= 0) {
          data.delete('photo')
        }

        $('#saveData').attr('disabled', true).text("Loading...");
        $.ajax({
          url: '<?php echo base_url('admin/pegawai/save') ?>',
          type: "POST",
          data: data,
          dataType: "json",
          cache: false,
          contentType: false,
          processData: false,
          error: function(res) {
            $('#saveData').attr('disabled', false).text("Save");
          },
          success: function(response, status, xhr, $form) {
            var d = response;
            $('#saveData').attr('disabled', false).text("Save");
            if (d.status == 'true') {
              Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              $("#cancelData").click();
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Gagal!',
                text: d.msg,
              })
            }
          }
        }, 'json');
      }
    });


    // tombol menampilkan list data
    $("#btnListData").click(function() {
      resetForm()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol batal tambah data
    $("#cancelData").click(function() {
      resetForm()
      loadTbl()
      $("#tableContainer").slideDown(500)
      $("#formContainer").slideUp(500)
    })


    // tombol tambah data
    $("#btnFormData").click(function() {
      $("#tableContainer").slideUp(500)
      $("#formContainer").slideDown(500)
    })


    // 
    $("#photo").change(function() {
      readURL()
      $(this).next().text($(this).val())
      $("#is_delete").val("no")
    })


    // 
    $("#hapusImage").click(function() {
      $("#photo").val("")
      $("#is_delete").val("yes")
      $("#photo").next().text("Choose file")
      $("#imagePreview").attr("src", "<?= base_url() ?>assets/global/images/profil.jpg")
    })


    // kategori pegawai select2
    $('#kp_id').select2()
  })
</script>