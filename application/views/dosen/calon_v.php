<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Calon Mahasiswa Bimbingan</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="control-label col-md-3">Jurusan</label>
                <div class="col-md-9">
                  <select name="f_jurusan_id" id="f_jurusan_id" class="form-control">
                    <?php foreach ($jurusan as $v) : ?>
                      <option value="<?= $v->jurusan_id ?>"><?= $v->jurusan_name ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="control-label col-md-3">Status</label>
                <div class="col-md-9">
                  <select name="f_status" id="f_status" class="form-control">
                    <option value="0">Proses</option>
                    <option value="2">Diterima</option>
                    <option value="1">Ditolak</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-sm table-hover" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Judul</th>
                <th class="text-center">Sinopsis</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // inisialisasi tabel
  dTableInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('dosen/calon/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          "data": function(d) {
            d.f_jurusan_id = $('#f_jurusan_id').val();
            d.f_status = $('#f_status').val();
          },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -2, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0, -2, -1],
            "className": "text-center"
          },
          // {
          //   "targets": [0],
          //   "width": "5%",
          // },
          // {
          //   "targets": [1],
          //   "width": "30%"
          // },
          // {
          //   "targets": [2],
          //   "width": "40%"
          // },
          // {
          //   "targets": [3],
          //   "width": "15%",
          // },
          // {
          //   "targets": [4],
          //   "width": "10%",
          // },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="<?= base_url() ?>assets/global/images/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();


  // fungsi muat ulang refrensi-table
  dLoadTbl = function() {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // fungsi hapus
  dSetVal = function(data) {
    const i = data.split('|')
    let msg = ''
    if (i[1] == 2) {
      msg = '<p>Data yang sudah <span class="badge badge-success">DISETUJUI</span>, tidak bisa dikembalikan!</p>'
    } else {
      msg = '<p>Data yang sudah <span class="badge badge-danger">DITOLAK</span>, tidak bisa dikembalikan!</p>'
    }
    Swal.fire({
      title: 'Apakah Anda yakin?',
      html: msg,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        data = {
          id: i[0],
          status: i[1],
        }
        if (i[1] == i[2]) {
          data.parent = {
            dt_id: i[3],
            dt_status: i[1],
          }
        }
        $.ajax({
          url: '<?= base_url() ?>dosen/calon/save',
          data: data,
          type: 'post',
          dataType: 'json',
          success: function(res) {
            if (res.status == 'true') {
              Swal.fire(
                'Berhasil!',
                'Verifikasi telah berhasil',
                'success'
              )
              dLoadTbl()
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  dResetForm = function() {
    $("#form_vendor")[0].reset()
    $("#form_vendor").validate().dResetForm()
    $("#jurusan_id,#jurusan_code_lama,#jurusan_name_lama").val('')
    $("#act").val('add')
  }


  $(document).ready(function() {
    dTableInit.init();

    $("#f_jurusan_id,#f_status").change(function() {
      dLoadTbl()
    })

  })
</script>