<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= !empty($title) ? $title . " | SIBIMTA" : "SIBIMTA" ?></title>
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/global/images/logo.ico'); ?>" />

  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/css/sb-admin-2.min.css" rel="stylesheet">
  <!-- <link href="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"> -->
  <link href="<?= base_url() ?>node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/jstree/dist/themes/default/style.min.css" rel="stylesheet">
  <style>
    .error {
      color: #e74a3b;
      border-color: #e74a3b;
      font-size: 1rem;
      width: 100%;
    }

    form .form-unit .select2-container,
    form .form-unit .select2-selection {
      width: 100% !important;
    }

    .form-unit .select2-container,
    .form-unit .select2-selection {
      width: 100%;
    }

    form .select2 {
      width: 100% !important;
    }

    /* .select2-selection__rendered {
    line-height: 1em !important;
  } */

    .select2-container .select2-selection--single {
      height: 35px !important;
    }

    .select2-selection__arrow {
      height: 34px !important;
    }
  </style>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="<?= base_url() ?>node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?= base_url() ?>node_modules/jquery-validation/dist/localization/messages_id.min.js"></script>
  <script src="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
  <script src="<?= base_url() ?>node_modules/select2/dist/js/select2.min.js"></script>
  <script src="<?= base_url() ?>node_modules/jstree/dist/jstree.min.js"></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">