<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
  </a>
  <?php $parent_menu_id = 0; ?>
  <?php foreach ($sidebar as $k => $v) : ?>
    <?php if ($v->menu_id != $parent_menu_id && $v->menu_level == 1) : ?>
      <?php $class = ($k == 0) ? 'my-0' : '' ?>
      <!-- Divider -->
      <hr class="sidebar-divider <?= $class ?>">
      <?php $parent_menu_id = $v->menu_id; ?>
    <?php endif; ?>

    <?php if ($v->jenis_menu == 1) : ?>
      <?php $active = $v->menu_id == $menu_id ? 'active' : '' ?>
      <li class="nav-item <?= $active ?>">
        <a class="nav-link pb-1" href="<?= base_url() . $v->menu_url ?>">
          <i class="<?= $v->menu_icon != '' ? $v->menu_icon : 'fa fa-snowflake'; ?>"></i>
          <span><?= $v->menu_name ?></span>
        </a>
      </li>
    <?php else : ?>
      <div class="sidebar-heading">
        <?= $v->menu_name ?>
      </div>
    <?php endif; ?>

  <?php endforeach; ?>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->