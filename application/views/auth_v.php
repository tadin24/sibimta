<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/global/images/logo.ico'); ?>" />

  <title>SIBIMTA - Login</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-6 col-lg-6 col-md-12">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                  </div>
                  <form class="user" method="POST" action="" id="form_login">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="username" name="username" placeholder="Masukkan Username">
                    </div>
                    <div class=" form-group">
                      <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Masukkan Password">
                    </div>
                    <input type="submit" class="btn btn-primary btn-user btn-block" id="login" value="Login">
                    <!-- <a href="javascript:void(0)" onclick="login()" id="login" class="btn btn-primary btn-user btn-block">
                      Login
                    </a> -->
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/js/sb-admin-2.min.js"></script>
  <script src="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
  <script>
    // function login() {

    // }
    $("#form_login").submit(function(e) {
      console.log('ok1');
      e.preventDefault()
      console.log('ok');
      $("#login").attr("disabled", "disabled").val("Loading...")
      $.ajax({
        url: '<?= base_url() ?>auth/login',
        data: {
          username: $("#username").val(),
          password: $("#password").val(),
        },
        dataType: 'json',
        type: 'post',
        error: function(res) {
          $("#login").removeAttr("disabled", "disabled").val("Login")
        },
        success: function(res) {
          if (!res.status) {
            $("#login").removeAttr("disabled", "disabled").val("Login")
            Swal.fire({
              icon: 'error',
              title: 'Gagal!',
              text: res.msg,
            })
          } else {
            window.location.href = res.url
          }
        }
      })
    })
  </script>

</body>

</html>