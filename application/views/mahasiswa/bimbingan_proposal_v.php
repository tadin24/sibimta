<div class="card">
  <div class="card-body">
    <div class="container side" id="bimbinganSide">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group row">
            <label class="control-label col-md-3">Dosen</label>
            <div class="col-md-9">
              <select id="f_target_id" class="form-control">
                <option value="1">AA</option>
                <option value="2">BB</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <button class="btn btn-info btn-sm" onclick="tambahPesan()">Tambah Pesan</button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="panel panel-info">
            <div class="panel-body scrollable-panel">
              <div class="row d-flex justify-content-center mx-0 my-1">
                <div class="col-md-12">
                  <div class="main-card mb-3 card">
                    <div class="card-body">
                      <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                        <div class="vertical-timeline-item vertical-timeline-element">
                          <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge badge-dot badge-dot-xl badge-warning"> </i> </span>
                            <div class="vertical-timeline-element-content bounce-in">
                              <p>Another meeting with UK client today, at <b class="text-danger">3:00 PM</b></p>
                              <p>Yet another one, at <span class="text-success">5:00 PM</span></p> <span class="vertical-timeline-element-date">12:25 PM</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container side" id="semproSide" style="display: none;">
      sempro
    </div>
  </div>
</div>

<div class="modal fade" id="pesanModal" tabindex="-1" aria-labelledby="pesanModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="pesanModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_vendor">
          <input type="hidden" name="target_id" id="target_id">
          <div class="form-group row">
            <label class="control-label col-md-3">Pesan</label>
            <div class="col-md-9">
              <textarea class="form-control" id="pesan" name="pesan" placeholder="Ketikkan pesan"></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="sendPesan">Kirim</button>
      </div>
    </div>
  </div>
</div>

<script>
  function tambahPesan() {
    $("#pesanModalLabel").text('Pesan ke : ' + $("#f_target_id option:selected").text())
    $("#target_id").val($("#f_target_id").val())
    $("#pesanModal").modal('show')
  }


  // fungsi hapus
  function set_del(id) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?= base_url() ?>admin/jurusan/hapus/' + id,
          success: function(res) {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
            }
          }
        })
      }
    })
  }


  // fungsi reset form
  function resetForm() {}


  function readURL(input) {
    if (input.files && input.files[0]) {
      // var reader = new FileReader();
      // reader.onload = function(e) {
      console.log(input);
      $(input).next('label').text($(input)[0].files.target.result);
      // }
    }
  }


  $(document).ready(function() {

    // menu daftar ta
    $("#dt_sinopsis").change(function() {
      // readURL(this)

      if (this.files && this.files[0]) {
        $(this).next('label').text($(this)[0].files[0].name);
      }
    })

    $("#saveDaftar").click(function() {
      let pesan = ''
      const error = {
        msg: [],
        n: 0,
      }

      if ($("#dt_judul").val() == '') {
        error.msg.push('Judul tidak boleh kosong!')
        error.n = error.n + 1
      }

      if ($("#dospem1").val() == '' || $("#dospem2").val() == '') {
        error.msg.push('Harus memilih dosen!')
        error.n = error.n + 1
      }

      if ($("#dospem1").val() == $("#dospem2").val()) {
        error.msg.push('Dospem 1 tidak boleh sama dengan Dospem 2!')
        error.n = error.n + 1
      }

      if ($("#dt_sinopsis")[0].files.length <= 0 && $("#dt_id").val() == '') {
        error.msg.push('Harus mengumpulkan file sinopsis!')
        error.n = error.n + 1
      }

      if (error.n > 0) {
        let msg = ''
        error.msg.forEach(function(i, index) {
          msg += `<label class="control-label">${i}</label>`
        })
        swal.fire({
          icon: 'error',
          html: msg,
          title: 'Gagal'
        })
      } else {
        const data = new FormData(document.getElementById('daftarForm'))
        if ($("#dt_sinopsis")[0].files.length <= 0) {
          data.delete('dt_sinopsis')
        }
        $.ajax({
          url: '<?= base_url() ?>mahasiswa/proposal/save',
          type: "POST",
          data: data,
          dataType: "json",
          cache: false,
          contentType: false,
          processData: false,
          success: function(res) {
            if (res.status == 'true') {
              Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Gagal!',
                html: res.msg
              })
            }

          }
        })
      }

    })
  })
</script>