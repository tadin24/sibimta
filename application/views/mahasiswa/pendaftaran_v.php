<div class="row">
  <div class="col-md-12">
    <form action="" id="daftarForm">
      <input type="hidden" name="dt_id" id="dt_id" value="<?= $mhs->dt_id ?>">
      <input type="hidden" name="d_mhs_id" id="d_mhs_id" value="<?= $mhs->mhs_id ?>">
      <input type="hidden" name="d_jurusan_id" id="d_jurusan_id" value="<?= $mhs->jurusan_id ?>">
      <div class="form-group row">
        <label class="control-label col-md-3">Nama</label>
        <label class="control-label col-md-9" id="mhs_nama"><?= $mhs->mhs_name ?></label>
      </div>
      <div class="form-group row">
        <label class="control-label col-md-3">NIM</label>
        <label class="control-label col-md-9" id="mhs_nim"><?= $mhs->mhs_nim ?></label>
      </div>
      <div class="form-group row">
        <label class="control-label col-md-3">Jurusan</label>
        <label class="control-label col-md-9" id="jurusan_name"><?= $mhs->jurusan_name ?></label>
      </div>
      <div class="form-group row">
        <label class="control-label col-md-3">Judul</label>
        <div class="col-md-9">
          <input type="text" name="dt_judul" id="dt_judul" class="form-control" value="<?= $mhs->dt_judul ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="control-label col-md-3">Dosen Pembimibng 1</label>
        <input type="hidden" name="detail[0][dtd_id]" value="<?= $mhs->dtd_id1 ?>">
        <div class="col-md-9">
          <select name="detail[0][dosen_id]" id="dospem1" class="form-control">
            <?php foreach ($dosen as $v) : ?>
              <?php
              if ($mhs->dospem1 == $v->pegawai_id) {
                $selected = 'selected';
              } else {
                $selected = '';
              }
              ?>
              <option value="<?= $v->pegawai_id ?>" <?= $selected ?>><?= $v->pegawai_nama ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="control-label col-md-3">Dosen Pembimibng 2</label>
        <div class="col-md-9">
          <input type="hidden" name="detail[1][dtd_id]" value="<?= $mhs->dtd_id2 ?>">
          <select name="detail[1][dosen_id]" id="dospem2" class="form-control">
            <?php foreach ($dosen as $v) : ?>
              <?php
              if ($mhs->dospem2 == $v->pegawai_id) {
                $selected = 'selected';
              } else {
                $selected = '';
              }
              ?>
              <option value="<?= $v->pegawai_id ?>" <?= $selected ?>><?= $v->pegawai_nama ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="control-label col-md-3">Sinopsis</label>
        <div class="col-md-6">
          <div class="custom-file">
            <input type="file" class="custom-file-input" accept="application/msword,application/pdf" id="dt_sinopsis" name="dt_sinopsis">
            <label class="custom-file-label" for="dt_sinopsis"><?= !empty($mhs->dt_sinopsis_name) ? $mhs->dt_sinopsis_name : 'Pilih File' ?></label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="button" class="btn btn-primary" id="saveDaftar">Save</button>
          <button type="button" class="btn btn-danger">Reset</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script>
  function readURL(input) {
    if (input.files && input.files[0]) {
      // var reader = new FileReader();
      // reader.onload = function(e) {
      console.log(input);
      $(input).next('label').text($(input)[0].files.target.result);
      // }
    }
  }


  $(document).ready(function() {

    // menu daftar ta
    $("#dt_sinopsis").change(function() {
      // readURL(this)

      if (this.files && this.files[0]) {
        $(this).next('label').text($(this)[0].files[0].name);
      }
    })

    $("#saveDaftar").click(function() {
      let pesan = ''
      const error = {
        msg: [],
        n: 0,
      }

      if ($("#dt_judul").val() == '') {
        error.msg.push('Judul tidak boleh kosong!')
        error.n = error.n + 1
      }

      if ($("#dospem1").val() == '' || $("#dospem2").val() == '') {
        error.msg.push('Harus memilih dosen!')
        error.n = error.n + 1
      }

      if ($("#dospem1").val() == $("#dospem2").val()) {
        error.msg.push('Dospem 1 tidak boleh sama dengan Dospem 2!')
        error.n = error.n + 1
      }

      if ($("#dt_sinopsis")[0].files.length <= 0 && $("#dt_id").val() == '') {
        error.msg.push('Harus mengumpulkan file sinopsis!')
        error.n = error.n + 1
      }

      if (error.n > 0) {
        let msg = ''
        error.msg.forEach(function(i, index) {
          msg += `<label class="control-label">${i}</label>`
        })
        swal.fire({
          icon: 'error',
          html: msg,
          title: 'Gagal'
        })
      } else {
        const data = new FormData(document.getElementById('daftarForm'))
        if ($("#dt_sinopsis")[0].files.length <= 0) {
          data.delete('dt_sinopsis')
        }
        $.ajax({
          url: '<?= base_url() ?>mahasiswa/pendaftaran/save',
          type: "POST",
          data: data,
          dataType: "json",
          cache: false,
          contentType: false,
          processData: false,
          success: function(res) {
            if (res.status == 'true') {
              Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Gagal!',
                html: res.msg
              })
            }

          }
        })
      }

    })
  })
</script>